/* --------------------------------------------------------------------------
    KNEXFILE                                                    Wosgrau
-------------------------------------------------------------------------- */
const conf = require('./config/api.conf.json')

/* -------------- MSSQL -------------- */
module.exports = {
    client: 'mssql',
    connection: {
      server  : conf.dbLicitacoes.server,
      database: conf.dbLicitacoes.database,
      user    : conf.dbLicitacoes.user,
      password: conf.dbLicitacoes.password
    },
    pool: {
      min: 2,
      max: 10
    },
    migrations: {
      tableName: 'knex_migrations'
    }
}

/* -------------- MYSQL -------------- */
/*module.exports = {
  client: 'mysql',
  connection: {
    server   : conf.dbAuditoria.server,
    database : conf.dbAuditoria.database,
    user     : conf.dbAuditoria.user,
    password : conf.dbAuditoria.password
  },
  pool: {
    min: 2,
    max: 10
  },
  migrations: {
    tableName: 'knex_migrations'
  }
}*/

/* -------------- POSTGRES -------------- */
/*module.exports = {
    client: 'postgresql',
    connection: {
      host    : conf.dbdbLicitacoes.hostname,
      database: conf.dbLicitacoes.database,
      user    : conf.dbLicitacoes.user,
      password: conf.dbLicitacoes.password
    },
    pool: {
      min: 2,
      max: 10
    },
    migrations: {
      tableName: 'knex_migrations'
    }
}*/