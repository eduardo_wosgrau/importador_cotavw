/* --------------------------------------------------------------------------
    FUNÇÕES                                                     Wosgrau
-------------------------------------------------------------------------- */
const conf                = require('./../config/api.conf.json');
const fsPath              = require('fs-path');
const path                = require('path');
const fs                  = require('fs')
const http                = require('http');
const moment              = require('moment');
const request             = require('request');
const zipper              = require('zip-local');
const htmlToPdf           = require('html-to-pdf');
const pdfreader           = require('pdfreader');
const pdf_table_extractor = require('pdf-table-extractor');
const PDFParser           = require('pdf2json');

const MSG_FALHA_CONVERTER_PDF_JSON       = `Falha ao converter arquivo PDF em Json.`;
const MSG_FALHA_OBTER_LINHAS_PDF         = `Falha ao obter linhas do arquivo PDF.`;
const MSG_FALHA_OBTER_TABELAS_PAGINA_PDF = `Falha ao obter tabela da página do arquivo PDF.`;
const MSG_FALHA_OBTER_ARQUIVOS_DIRETORIO = `Falha ao obter arquivos do diretório.`;
const MSG_FALHA_VERIFICAR_VALOR_BETWEEN  = `Falha ao verificar valor between.`;

exports.somenteNumeros = function(texto){
    try{
        texto = texto.replace(/[^\d]+/g,'');
    }catch(error){
        console.log(error);
    }finally{
        return texto;
    };
};

exports.get32bitsHash = function(){
    try{
        var hash = ''
        hash     = Math.random().toString(36).substring(2, 15) + Math.random().toString(36).substring(2, 15) + Math.random().toString(36).substring(2, 15);
        hash     = hash.substring(0, 32);
    }catch(error){
        console.log(error);
    }finally{
        return hash;
    };
};

exports.escreveArq = async function(caminho, conteudo){
    return new Promise(resolve => {
        try{
            fsPath.writeFile(caminho, conteudo, "utf8", function(err){
                if (err){
                    console.log(err);
                    resolve(false);
                }else
                    resolve(true);
            });
        }catch(error){
            resolve(false);
        };
    });
};

exports.sleep = function(ms){
    return new Promise(resolve => {
        setTimeout(resolve, ms);
    });
};

exports.limpaConteudoTag = function(texto, manterQuebraUnica){
    if (manterQuebraUnica == undefined)
        manterQuebraUnica = false;
    var tag      = "";
    var txtLimpo = texto.replace(/\&nbsp;/g, " ")      // REMOVE "&nbsp;"
                        .replace(/\&gt;/g, " ")        // REMOVE "&gt;"
                        .replace(/<br\s*\/?>/mg, " "); // REMOVE "<br>" E "<br/>"
    // \t \r e \n
    if (manterQuebraUnica){
        // \t
        while(txtLimpo.indexOf("\t\t") > -1)
            txtLimpo = txtLimpo.replace("\t\t", "\t");
        while(txtLimpo.indexOf("\t \t") > -1)
            txtLimpo = txtLimpo.replace("\t \t", "\t");
        // \r
        while(txtLimpo.indexOf("\r\r") > -1)
            txtLimpo = txtLimpo.replace("\r\r", "\r");
        while(txtLimpo.indexOf("\r \r") > -1)
            txtLimpo = txtLimpo.replace("\r \r", "\r");
        // \n
        while(txtLimpo.indexOf("\n\n") > -1)
            txtLimpo = txtLimpo.replace("\n\n", "\n");
        while(txtLimpo.indexOf("\n \n") > -1)
            txtLimpo = txtLimpo.replace("\n \n", "\n");
    }else{
        txtLimpo = txtLimpo.replace(/\t/g, " ")
                           .replace(/\r/g, " ")
                           .replace(/\n/g, " ")
    };
    // ESPAÇO DUPLO
    while(txtLimpo.indexOf("  ") > -1)
        txtLimpo = txtLimpo.replace("  ", " ");
    // LIMPA ESPAÇO NO COMEÇO
    if (txtLimpo.indexOf(" ") == 0)
        txtLimpo = txtLimpo.substring(1, txtLimpo.length);
    // TAGS
    while (txtLimpo.indexOf("<") > -1){
        tag = txtLimpo.substring((txtLimpo.indexOf("<") + 1), (txtLimpo.indexOf(" ", txtLimpo.indexOf("<"))));
        txtLimpo = txtLimpo.substring(0, txtLimpo.indexOf("<")) + 
        txtLimpo.substring((txtLimpo.indexOf("</" + tag) + tag.length + 3), txtLimpo.length);
    };    
    return txtLimpo;
};

exports.retiraEspacos = function(texto){
    while(texto.indexOf("  ") > -1)
        texto = texto.replace(" ", "");
    return texto;
};

exports.limpaCaracEspec = function(texto){
    return texto.replace(/\u0021/g, "") // REMOVE "!"
                .replace(/\u0023/g, "") // REMOVE "#"
                .replace(/\u0026/g, "") // REMOVE "&"
                .replace(/\u0028/g, "") // REMOVE "("
                .replace(/\u0029/g, "") // REMOVE ")"
                .replace(/\u002A/g, "") // REMOVE "*"
                .replace(/\u002B/g, "") // REMOVE "+"
                .replace(/\u002C/g, "") // REMOVE ","
                .replace(/\u002D/g, "") // REMOVE "-"
                .replace(/\u002E/g, "") // REMOVE "."
                .replace(/\u003A/g, "") // REMOVE ":"
                .replace(/\u003B/g, "") // REMOVE ";"
                .replace(/\u003C/g, "") // REMOVE "<"
                .replace(/\u003D/g, "") // REMOVE "="
                .replace(/\u003E/g, "") // REMOVE ">"
                .replace(/\u003F/g, "") // REMOVE "?"
                .replace(/\u0040/g, "") // REMOVE "@"
                .replace(/\u005B/g, "") // REMOVE "["
                .replace(/\u005C/g, "") // REMOVE "\"
                .replace(/\u0025/g, "") // REMOVE "%"
                .replace(/\u005D/g, "") // REMOVE "]"
                .replace(/\u005E/g, "") // REMOVE "^"
                .replace(/\u005F/g, "") // REMOVE "_"
                .replace(/\u0060/g, "") // REMOVE "`"
                .replace(/\u007B/g, "") // REMOVE "{"
                .replace(/\u007C/g, "") // REMOVE "|"
                .replace(/\u007D/g, "") // REMOVE "}"
                .replace(/\u007E/g, "") // REMOVE "~"
                .replace(/\u002F/g, "") // REMOVE "/"
};

exports.criarArray = function(linhas, colunas, valorDefault){
    var arr = [];
    // LINHAS
    for(var i = 0; i < linhas; i++){      
        // CRIA LINHA VAZIA
        arr.push([]);
        // ADICIONA COLUNAS À LINHA VAZIA
        arr[i].push(new Array(colunas));
        // VALOR DEFAULT
        if (valorDefault)
            for(var j = 0; j < colunas; j++)
                arr[i][j] = valorDefault;
    };
    return arr;
};

exports.textToDateTipo1 = function(texto){
    // (EX.: 22 fev 2018". -> 22/02/2018)
    var data = "";
    var mes  = "";
    texto = texto.trim();
    // DIA
    data  = texto.substring(0, texto.indexOf(" ")) + "/";
    texto = texto.substring((texto.indexOf(" ") + 1), texto.length);
    // MÊS
    mes   = texto.substring(0, texto.indexOf(" "));
    texto = texto.substring((texto.indexOf(" ") + 1), texto.length);
    switch(mes.toUpperCase()) {
        case 'JAN': mes = "01"; break;
        case 'FEV': mes = "02"; break;
        case 'MAR': mes = "03"; break;
        case 'ABR': mes = "04"; break;
        case 'MAI': mes = "05"; break;
        case 'JUN': mes = "06"; break;
        case 'JUL': mes = "07"; break;
        case 'AGO': mes = "08"; break;
        case 'SET': mes = "09"; break;
        case 'OUT': mes = "10"; break;
        case 'NOV': mes = "11"; break;
        case 'DEZ': mes = "12"; break;
        default: mes = "";
    };
    // ANO
    data = data + mes + "/" + texto;
    return data;
};

exports.textToDateTipo2 = function(texto){
    // (EX.: Segunda-Feira, 3 de Setembro de 2018. -> 03/09/2018)
    // RETIRA DIA DA SEMANA
    texto = texto.substring((texto.indexOf(",") + 2), texto.length);
    // DIA
    var dia = texto.substring(0, texto.indexOf(" de"));
    if (dia.length < 2)
        dia = "0" + dia;
    texto = texto.substring((texto.indexOf(" de") + 4), texto.length);
    // MÊS
    var mes = texto.substring(0, texto.indexOf(" de"));
    texto = texto.substring((texto.indexOf(" de") + 4), texto.length);
    switch(mes.toUpperCase()){
        case "JANEIRO":   mes = "01"; break;
        case "FEVEREIRO": mes = "02"; break;
        case "MARÇO":     mes = "03"; break;
        case "ABRIL":     mes = "04"; break;
        case "MAIO":      mes = "05"; break;
        case "JUNHO":     mes = "06"; break;
        case "JULHO":     mes = "07"; break;
        case "AGOSTO":    mes = "08"; break;
        case "SETEMBRO":  mes = "09"; break;
        case "OUTUBRO":   mes = "10"; break;
        case "NOVEMBRO":  mes = "11"; break;
        case "DEZEMBRO":  mes = "12"; break;
    };
    // ANO
    var ano = texto;
    // FORMATA DATA (00/00/0000)
    var data = dia + "/" + mes + "/" + ano;
    // RETIRA PONTOS E ESPAÇOS
    data = data.replace(/\u002E/g, "").replace(/" "/g, "");
    return data;
};

exports.getDateDiff = function(Log, dtInicio, dtFim){
    // FORMATO HH:MM:SS (< 24HRS)
    try{
        var dtIniStr = moment(dtInicio).format(`DD/MM/YYYY HH:mm:ss`);
        var dtFimStr = moment(dtFim).format(`DD/MM/YYYY HH:mm:ss`);
        return moment.utc(moment(dtFimStr,`DD/MM/YYYY HH:mm:ss`).diff(moment(dtIniStr,`DD/MM/YYYY HH:mm:ss`))).format("HH:mm:ss")
    }catch(error){
        Log.add(`Falha ao obter diferença entre datas. Dt. Inicial: ${dtIniStr} Dt. Final: ${dtFimStr} Erro: ${error}`, `E`);
    };
};

exports.getStringToMoment = function(Log, strData){
    try{
        return moment(strData, `DD/MM/YYYY HH:mm:ss`);
    }catch(error){
        Log.add(`Falha ao converter string para Moment. Erro: ${error}`, `E`);
    };
};

exports.getMomentToDate = function(Log, dataMoment){
    try{
        return dataMoment.toDate();
    }catch(error){
        Log.add(`Falha ao converter moment para DateTime. Erro: ${error}`, `E`);
    };
};

exports.getDateIso = function(Log, data){
    try{
        return moment(data).format("YYYY-MM-DD HH:mm:ss");
    }catch(error){
        Log.add(`Falha ao converter Date para ISO. Erro: ${error}`, `E`);
    };
};

exports.getStringToDateISO = function(Log, strData){
    try{
        var data = exports.getStringToMoment(Log, strData);
        return exports.getDateIso(Log, data);
    }catch(error){
        Log.add(`Falha ao converter string para DateTime. Erro: ${error}`, `E`);
    };
};

exports.getDecimalParaInteiro = function(Log, strDecimal){
    try{
        var nroInteiro;
        if (strDecimal.indexOf('.') >= 0)
            strDecimal = strDecimal.split(".").join("");
        if (strDecimal.indexOf(',') >= 0)
            strDecimal = strDecimal.substring(0, strDecimal.indexOf(","));
        nroInteiro = parseInt(strDecimal);
        return nroInteiro;
    }catch(error){
        Log.add(`Falha ao montar sumário da execução. Erro: ${error}`, `E`);
        return nroInteiro;
    };
};

exports.getDataAtual = function(){
    return exports.getSomenteDataString(new Date());
};

exports.somarDiasData = function(data, dias){
    try{
        data.setDate(data.getDate() + dias);
    }catch(error){
        console.log(error);
    }finally{
        return data;
    };
};

exports.getSomenteDataString = function(data){
    try{
        var somenteData = '';
        var dd          = String(data.getDate()).padStart(2, '0');
        var mm          = String(data.getMonth() + 1).padStart(2, '0'); //January is 0!
        var yyyy        = data.getFullYear();
        somenteData     = dd + '/' + mm + '/' + yyyy;
    }catch(error){
        console.log(error);
    }finally{
        return somenteData;
    };
};

exports.getDownloadPath = function(classRobo, subFolder = ''){
    try{
        var downloadsDir = '';
        // PASTA ARQ
        var caminhoFiles = path.dirname(require.main.filename);
        downloadsDir     = caminhoFiles.replace('files', 'arq');
        if (!fs.existsSync(downloadsDir))
            fs.mkdirSync(downloadsDir);
        // CLASSE ROBÔ
        if (downloadsDir.indexOf('/') >= 0)
            downloadsDir += '/' + classRobo
        else
            downloadsDir += '\\' + classRobo;
        if (!fs.existsSync(downloadsDir))
            fs.mkdirSync(downloadsDir);
        // SUBFOLDER
        if ((subFolder != undefined) && (subFolder != '')){
            if (downloadsDir.indexOf('/') >= 0)
                downloadsDir += '/' + subFolder
            else
                downloadsDir += '\\' + subFolder;
            if (!fs.existsSync(downloadsDir))
                fs.mkdirSync(downloadsDir);
        };
        if (downloadsDir.indexOf('/') >= 0)
            downloadsDir += '/'
        else
            downloadsDir += '\\';
    }catch(error){
        console.log(error);
    }finally{
        return downloadsDir;
    };
};

exports.excluirArquivo = function(Log, nomeArquivo){
    try{
        fs.unlinkSync(nomeArquivo);
        return true;
    }catch(error){
        Log.add(`Falha o excluir arquivo. Erro: ${error}`, `E`);
        return false;
    };
};

exports.arquivoExiste = function(caminhoArq){
    try{
        return fs.existsSync(caminhoArq);
    }catch(error){
        return false;
    };
};

exports.ziparPasta = async function(Log, caminhoPasta, caminhoPastaZip){
    try{
        var zipou = false;
        zipper.sync.zip(caminhoPasta).compress().save(caminhoPastaZip);
        zipou = true;
    }catch(error){
        zipou = false;
        Log.add(`Falha ao zipar pasta "${caminhoPasta}". Erro: ${error}`, `E`);
    }finally{
        return zipou;
    };
};

exports.arquivosDaPagina = async function(Log, caminhoPasta){
    try{
        var Arquivos = [];
        await new Promise((resolve, reject) => {
            try{
                if (exports.arquivoExiste(caminhoPasta)){
                    fs.readdir(caminhoPasta, function (error, files) {
                        if (error)
                            console.log(`${MSG_FALHA_OBTER_ARQUIVOS_DIRETORIO} Erro: ${error}`);
                        files.sort(function(a, b) {
                            return fs.statSync(caminhoPasta + a).mtime.getTime() - fs.statSync(caminhoPasta + b).mtime.getTime();
                        });
                        files.forEach(function (file){
                            if (file.indexOf('.crdownload') >= 0)
                                file = file.replace('.crdownload', '');
                            Arquivos.push(file);
                        });
                        resolve(true);
                    });
                };
            }catch(error){
                console.log(error);
                reject(error);
            };
        });
    }catch(error){
        Log.add(`${MSG_FALHA_OBTER_ARQUIVOS_DIRETORIO}. Erro: ${error}`, `E`);
    }finally{
        return Arquivos;
    };
};

exports.validarCNPJ = function(cnpj){
    try{
        if ((cnpj == undefined) || (cnpj == null) || (cnpj.trim() == ''))
            return false;
        cnpj = cnpj.replace(/[^\d]+/g,'');
        if (cnpj.length != 14)
            return false;
        if ((cnpj == "00000000000000") || (cnpj == "11111111111111") || (cnpj == "22222222222222") || 
            (cnpj == "33333333333333") || (cnpj == "44444444444444") || (cnpj == "55555555555555") || 
            (cnpj == "66666666666666") || (cnpj == "77777777777777") || (cnpj == "88888888888888") || 
            (cnpj == "99999999999999"))
            return false;
        // VALIDA DÍGITOS VERIFICADORES
        tamanho = (cnpj.length - 2);
        numeros = cnpj.substring(0, tamanho);
        digitos = cnpj.substring(tamanho);
        soma    = 0;
        pos     = (tamanho - 7);
        for (i = tamanho; i >= 1; i--){
            soma += numeros.charAt(tamanho - i) * pos--;
            if (pos < 2)
                pos = 9;
        };
        resultado = soma % 11 < 2 ? 0 : 11 - soma % 11;
        if (resultado != digitos.charAt(0))
            return false;
        tamanho = (tamanho + 1);
        numeros = cnpj.substring(0, tamanho);
        soma    = 0;
        pos     = (tamanho - 7);
        for (i = tamanho; i >= 1; i--){
            soma += numeros.charAt(tamanho - i) * pos--;
            if (pos < 2)
                pos = 9;
        };
        resultado = soma % 11 < 2 ? 0 : 11 - soma % 11;
        if (resultado != digitos.charAt(1))
            return false;
        return true;
    }catch(error){
        console.log(error);
        return false;
    };
};

exports.vazioParaNull = function(texto){
    try{
        if ((texto == undefined) || (texto == ''))
            texto = null;
    }catch(error){
        console.log(error);
    }finally{
        return texto;
    };
};

exports.setLogSumarioLicitacoes = function(Log, nroInseridas, nroAtualizadas, nroIgnoradas, nroFalhas, nroTotal, final = false){
    try{
        var msg = `${nroInseridas} Inserida(s) | ${nroAtualizadas} Atualizada(s) | ` + 
                  `${nroIgnoradas} Ignorada(s) | ${nroFalhas} Falha(s) | Total ${nroTotal}`;
        if (final)
            msg = `Execução finalizada. ${msg}`;
        Log.add(msg, `I`);
    }catch(error){
        Log.add(`Falha ao montar sumário da execução. Erro: ${error}`, `E`);
    };
};

exports.isNumeric = function(Log, texto){
    try{
        var isNumeric = false;
        texto         = texto.replace(/,/g, '.');
        isNumeric     = (!isNaN(texto));
    }catch(error){
        isNumeric = false;
        Log.add(`Falha ao validar numérico. Erro: ${error}`, `E`);
    }finally{
        return isNumeric;
    };
};

exports.getProxyRequest = function(Log, proxied = useProxy){
    try{
        var proxiedRequest;
        if (!proxied)
            proxiedRequest = request
        else if ((conf.proxy.host != undefined) && (conf.proxy.host != '') &&
                 (conf.proxy.port != undefined) && (conf.proxy.port != '') &&
                 (conf.proxy.user != undefined) && (conf.proxy.user != '') &&
                 (conf.proxy.password != undefined) && (conf.proxy.password != '')){
            var proxyUrl   = `http://${conf.proxy.user}:${conf.proxy.password}@${conf.proxy.host}:${conf.proxy.port}`;
            proxiedRequest = request.defaults({'proxy': proxyUrl});
        };
    }catch(error){
        Log.add(`Falha ao gerar proxied request. Erro: ${error}`, `E`);
    }finally{
        return proxiedRequest;
    };
};

exports.downloadFile = function(Log, url, nomeArq){
    return new Promise((resolve, reject) => {
        try{
            const file    = fs.createWriteStream(nomeArq);
            const request = http.get(url, (response) => {
                if (response.statusCode == 200)
                    response.pipe(file)
                else
                    reject('Requisição GET falhou');
            });
        
            // close() is async, call cb after close completes
            file.on('finish', () => file.close(() => {resolve(true)}));
        
            // check for request error too
            /*request.on('error', (err) => {
                fs.unlink(nomeArq);
                reject(err.message);
                return;
            });
        
            file.on('error', (err) => { // Handle errors
                fs.unlink(nomeArq); // Delete the file async. (But we don't check the result) 
                //reject(err.message);
                //return;
            });*/
        }catch(error){
            Log.add(`Falha ao baixar documento. Erro: ${error}`, `E`);
            resolve(false);
        };
    });
};

exports.apagarPastaRecursiva = function(Log, caminho){
    try{
        var apagou = false;
        if (fs.existsSync(caminho)){
            fs.readdirSync(caminho).forEach(function(file, index){
                var caminhoAtual = caminho + "/" + file;
                if (fs.lstatSync(caminhoAtual).isDirectory())
                    apagarPastaRecursiva(Log, caminhoAtual)
                else
                    fs.unlinkSync(caminhoAtual);
            });
            fs.rmdirSync(caminho);
            apagou = true;
        };
    }catch(error){
        apagou = false;
        Log.add(`Falha ao excluir pasta "${caminho}" recursivamente. Erro: ${error}`, `E`);
    }finally{
        return apagou;
    };
};

exports.salvarPdfHtml = async function(Log, html, caminhoPdf){
    try{
        var salvouPdf = false;
        htmlToPdf.setInputEncoding('UTF-8');
        htmlToPdf.setOutputEncoding('UTF-8');
        var promessa = new Promise(async (resolve, reject) => {
            try{
                htmlToPdf.convertHTMLString(html, caminhoPdf,
                    function (error, success) {
                        if (error)
                            resolve(false)
                        else
                            resolve(true);
                    }
                );
            }catch(error){
              resolve(false);
            };
          });
          salvouPdf = await Promise.resolve(promessa);
    }catch(error){
        salvouPdf = false;
        Log.add(`Falha ao salvar PDF do HTML. Erro: ${error}`, `E`);
    }finally{
        return salvouPdf;
    };
};

exports.getCampo = function(Log, Campos, nomeCampo){
    try{
        var valorCampo = '';
        for (let count = 0; count < Campos.length; count++)
            if (Campos[count][0].trim().toUpperCase() == nomeCampo.trim().toUpperCase()){
                var valor = Campos[count][1];
                if (typeof valor == 'boolean')
                    valorCampo = valor
                else
                    valorCampo = Campos[count][1].trim();
                break;
            };
    }catch(error){
        Log.add(`Falha ao obter valor do campo "${nomeCampo}". Erro: ${error}`, `E`);
    }finally{
        return valorCampo;
    };
};

exports.getLinhasPDF = async function(Log, caminhoPdf){
    try{
        var Linhas = [];
        await new Promise((resolve, reject) => {
            new pdfreader.PdfReader().parseFileItems(caminhoPdf, function(err, item) {
                if (err){
                    console.log(err)
                    resolve(false);
                }else if (!item)
                    resolve(true)
                else if (item.text)
                    Linhas.push(item.text);
            });
        });
    }catch(error){
        Log.add(`${MSG_FALHA_OBTER_LINHAS_PDF}. Erro: ${error}`, `E`);
    }finally{
        return Linhas;
    };
};

exports.getTabelaPaginaPDF = async function(Log, caminhoPdf, nroPaginaPDF){
    try{
        var Tabela = [];
        if ((caminhoPdf == undefined) || (caminhoPdf == null) || (caminhoPdf == '')){
            Log.add(`${MSG_FALHA_OBTER_TABELAS_PAGINA_PDF} Caminho do arquivo PDF inválido.`, `E`);
            return Tabela;
        };
        if ((nroPaginaPDF == undefined) || (nroPaginaPDF == null) || (nroPaginaPDF < 0)){
            Log.add(`${MSG_FALHA_OBTER_TABELAS_PAGINA_PDF} Número da página do PDF inválido.`, `E`);
            return Tabela;
        };
        await new Promise((resolve, reject) => {
            function success(result){
                if (result.pageTables[nroPaginaPDF].tables != undefined)
                    Tabela = result.pageTables[nroPaginaPDF].tables;
                resolve(true);
            };
            function error(err){
                Log.add(`${MSG_FALHA_OBTER_TABELAS_PAGINA_PDF}. Erro: ${err}`, `E`);
                resolve(false);
            };
            pdf_table_extractor(caminhoPdf, success, error);
        });
    }catch(error){
        Log.add(`${MSG_FALHA_OBTER_TABELAS_PAGINA_PDF}. Erro: ${error}`, `E`);
    }finally{
        return Tabela;
    };
};

exports.pdfToJson = async function(Log, caminhoPdf){
    try{
        var json = {};
        if ((caminhoPdf == undefined) || (caminhoPdf == null) || (caminhoPdf == '')){
            Log.add(`${MSG_FALHA_CONVERTER_PDF_JSON}. Erro: ${error}`, `E`);
            return json;
        };
        await new Promise((resolve, reject) => {
            let pdfParser = new PDFParser(this, 1);
            pdfParser.on("pdfParser_dataError", errData => console.error(errData.parserError) );
            pdfParser.on("pdfParser_dataReady", pdfData => {
                //fs.writeFileSync('C:\\Prog\\gelic\arq\\VwFileReader\\exports\\1.json', JSON.stringify(pdfData));
                //fs.writeFileSync('C:\\Prog\\gelic\\arq\\VwFileReader\\exports\\2.txt', pdfParser.getRawTextContent());
                json = pdfData;
                resolve(true);
            });
            pdfParser.loadPDF(caminhoPdf);
        });
        /*var json = {};
        var rows = {}; // indexed by y-position
        
        function printRows() {
            Object.keys(rows) // => array of y-positions (type: float)
                .sort((y1, y2) => parseFloat(y1) - parseFloat(y2)) // sort float positions
                .forEach(y => console.log((rows[y] || []).join("")));
        }
        
        new pdfreader.PdfReader().parseFileItems(caminhoPdf, function(
            err,
            item
        ) {
        if (!item || item.page) {
            // end of file, or page
            printRows();
            console.log("PAGE:", item.page);
            rows = {}; // clear rows for next page
        } else if (item.text) {
            // accumulate text items into rows object, per line
            (rows[item.y] = rows[item.y] || []).push(item.text);
        }
        });*/
    }catch(error){
        Log.add(`${MSG_FALHA_CONVERTER_PDF_JSON}. Erro: ${error}`, `E`);
    }finally{
        return json;
    };
};

exports.numericBetween = function(Log, valor, valorIni, valorFim, inclusivo){
    try{
        var isBetween = false;
        var min       = Math.min.apply(Math, [valorIni, valorFim]);
        var max       = Math.max.apply(Math, [valorIni, valorFim]);
        isBetween     = (inclusivo ? ((valor >= min) && (valor <= max)) : ((valor > min) && (valor < max)));
    }catch(error){
        Log.add(`${MSG_FALHA_VERIFICAR_VALOR_BETWEEN}. Erro: ${error}`, `E`);
    }finally{
        return isBetween;
    };
};

exports.valorToDecimalISO = function(Log, valor = ''){
    try{
        var valorISO = '';
        if ((valor == undefined) || (valor == null))
            return valorISO;
        valorISO = valor.replace('R$', '');
        valorISO = valorISO.replace(/\./g, '');
        valorISO = valorISO.replace(/\,/g, '.');
        valorISO = valorISO.trim();
    }catch(error){
        Log.add(`${MSG_FALHA_CONVERTER_VALOR_ISO} Erro: ${error}`, `E`);
    }finally{
        return valorISO;
    };
};