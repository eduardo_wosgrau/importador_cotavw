/* --------------------------------------------------------------------------
    FUNÇÕES DOM                                                Wosgrau
-------------------------------------------------------------------------- */
const funcoes   = require(`./funcoes.js`);
const jsdom     = require(`jsdom`);
const { JSDOM } = jsdom;

exports.getDOMFromRequest = async function(Log, optionsRequest, usarProxy = useProxy){
    try{
        var objDom;
        var promessa = new Promise(async (resolve, reject) => {
            var objRequest = funcoes.getProxyRequest(Log, usarProxy);
            objRequest(optionsRequest, function (error, response, body) {
                try{
                    if (error)
                        reject(error)
                    else if (response.statusCode == 200){
                        if (response.body != undefined)
                            resolve(response.body)
                        else
                            reject('');
                    };
                }catch(err){
                    reject(err);
                };
            });
        });
        await Promise.all([promessa])
        .then(results => {
             objDom = exports.getDOMFromHtml(Log, results[0]);
        })
        .catch(error => { 
            throw error;
        });
    }catch(error){
        console.log(error);
        Log.add(`Falha ao obter DOM. URL: ${url}. Erro ${error}`, `E`);
    }finally{
        return objDom;
    };
};

exports.getDOMFromHtml = function(Log, contentHtml){
    try{
        var objDom;
        objDom = new JSDOM(contentHtml);
    }catch(error){
        console.log(error);
        Log.add(`Falha ao gerar DOM do HTML. Erro ${error}`, `E`);
    }finally{
        return objDom;
    };
};