/* --------------------------------------------------------------------------
    FUNÇÕES SQL SERVER                                          Wosgrau
-------------------------------------------------------------------------- */
const Sequelize = require('sequelize');
const config    = require('../config/api.conf.json');

exports.executeQuery = async function(Log, qryConsulta){
    try{
        var consultou = false;
        var sequelize = new Sequelize(config[qryConsulta.nomeConfDb].database,
                                      config[qryConsulta.nomeConfDb].user, 
                                      config[qryConsulta.nomeConfDb].password, 
                                      {host: config[qryConsulta.nomeConfDb].server, dialect: 'mssql'});
        await sequelize.query(qryConsulta.query, 
                              {replacements : qryConsulta.params},
                              {type: sequelize.QueryTypes.SELECT})
        .then(result => {
            qryConsulta.data = result;
            consultou        = true;
        }).catch(error => {
            Log.add(`${qryConsulta.msgErro} Erro: ${error}`, `E`);
            consultou = false;
        });
    }catch(error){
        Log.add(`${qryConsulta.msgErro} Erro: ${error}`, `E`);
        consultou = false;
    }finally{
        return consultou;
    };
};  

exports.SetIntervalQuery = function(Log, query, limit, offset){
    try{
        if ((query == undefined) || (query == null) || (query == '')){
            Log.add(`Falha ao setar intevalo (Limit e Offset) da query. Query inválida.`, `E`);
            return query;
        };
        if ((limit == undefined) || (limit == null) || (limit < 0)){
            Log.add(`Falha ao setar intevalo (Limit e Offset) da query. Limit inválido.`, `E`);
            return query;
        };
        if ((offset == undefined) || (offset == null) || (offset < 0)){
            Log.add(`Falha ao setar intevalo (Limit e Offset) da query. Offset inválido.`, `E`);
            return query;
        };
        query += `\n OFFSET ${offset} ROWS FETCH NEXT ${limit} ROWS ONLY`;
    }catch(error){
        Log.add(`Falha ao setar intevalo (Limit e Offset) da query. Erro: ${error}`, `E`);
    }finally{
        return query;
    };
};

/*
const mssql = require('mssql');

exports.executeQuery = async function(Log, bdAcessData, query, priorizarEnvVars = false){
    try{
        var resultado;
        var dataConnection = {
            server  : bdAcessData.server,
            database: bdAcessData.database,
            user    : bdAcessData.user,
            password: bdAcessData.password
        };
        if ((priorizarEnvVars) && 
            (process.env.db_host != undefined) && (process.env.db_database != undefined) && 
            (process.env.db_user != undefined) && (process.env.db_host != undefined)){
            dataConnection.server   = process.env.db_server;
            dataConnection.database = process.env.db_database;
            dataConnection.user     = process.env.db_user;
            dataConnection.password = process.env.db_password;
        };
        for (let count = 0; count < 5; count++){
            try{
                const config = {
                    user    : dataConnection.user,
                    password: dataConnection.password,
                    server  : dataConnection.server,
                    database: dataConnection.database
                };
                mssql.connect(config).then(() => {
                    return mssql.query(query);
                }).then(result => {
                    resultado = result;
                }).catch(err => {
                    console.log(err);
                });
            }catch(error){
                console.log(error);
            }finally{
                if (resultado)
                    break;
            };
        };
    }catch(error){
        Log.add(`Falha ao executar query. Erro: ${error}`, `E`);
    }finally{
        return resultado;
    };
};*/