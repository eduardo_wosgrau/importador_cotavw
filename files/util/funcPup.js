/* --------------------------------------------------------------------------
    FUNÇÕES PUPPETEER                                           Wosgrau
-------------------------------------------------------------------------- */
const puppeteer      = require("puppeteer");
const func           = require("./funcoes.js");
const conf           = require('./../config/api.conf.json');
const {TimeoutError} = require('puppeteer/Errors');

const MSG_FALHA_BAIXAR_DOCUMENTO = `Falha ao baixar documento (browser).`;

exports.getTamanhoBySelector = async function(Log, escopo, selector, nomeAcao){
    try{
        await exports.esperarBySelector(escopo, selector, 5000);
        return await escopo.evaluate((selector) => {
            return document.querySelectorAll(selector).length;
        }, selector);
    }catch(error){
        Log.add(`Falha ao obter quantidade de "${nomeAcao}". Selector: "${selector} Erro: ${error}`, `E`);
    };
};

exports.getTamanhoByClassName = async function(Log, escopo, className, waitSelector){    
    try{
        if ((waitSelector) && (waitSelector != ""))
            await exports.esperarBySelector(escopo, waitSelector);
        var tamanho = 0;
        tamanho = await escopo.evaluate((className) => {
            return document.getElementsByClassName(className).length;
        }, className);
        return tamanho;
    }catch(error){
        Log.add(`Falha ao obter tamanho. ClassName: "${className}". Erro: ${error}`, `E`);
    };
};

exports.setBySelector = async function(Log, escopo, selector, valor, nomeCampo){
    try{
        await escopo.waitFor(selector);
        await escopo.type(selector, valor);
    }catch(error){
        Log.add(`Falha ao preencher campo "${nomeCampo}". Selector: "${selector}". Erro: ${error}`, `E`);
    };
};

exports.setById = async function(Log, escopo, id, valor, nomeCampo){
    try{
        let selector = "#" + id;
        return await exports.setBySelector(Log, escopo, selector, valor, nomeCampo);
    }catch(error){
        Log.add(`Falha ao preencher campo "${nomeCampo}". Id: "${id}". Erro: ${error}`, `E`);
    };
};

exports.setComboBySelector = async function(Log, escopo, selector, valor, nomeCampo){
    try{
        await escopo.select(selector, valor.toString());
    }catch(error){
        Log.add(`Falha ao preencher combobox "${nomeCampo}". Selector: "${selector}". Erro: ${error}`, `E`);
    };
};

exports.setComboById = async function(Log, escopo, id, valor, nomeCampo){
    try{
        let selector = "#" + id;
        return await exports.setComboBySelector(Log, escopo, selector, valor, nomeCampo);
    }catch(error){
        Log.add(`Falha ao preencher combobox "${nomeCampo}". Id: "${id}". Erro: ${error}`, `E`);
    };
};

exports.setComboByName = async function(Log, escopo, name, index, valor, nomeCampo){
    try{
        await escopo.evaluate((name, index, valor) => {
            function indexMatchingText(elem, optionValue) {
                for (var i = 0; i < elem.children.length; i++)
                    if (elem.children[i].value == optionValue)
                        return i;
                return undefined;
            };
            document.getElementsByName(name)[index].selectedIndex = 
                indexMatchingText(document.getElementsByName(name)[index], valor);
        }, name, index, valor);
    }catch(error){
        Log.add(`Falha ao preencher combobox "${nomeCampo}". Name: "${name}". Erro: ${error}`, `E`);
    };
};

exports.clickBySelector = async function(Log, escopo, selector, nomeAcao, index){
    try{
        await exports.esperarBySelector(escopo, selector, 5000);
        if (index)
            return await escopo.evaluate((selector, index) => {
                document.querySelectorAll(selector)[index].click();
            }, selector, index);
        else
            await escopo.click(selector);
    }catch(error){
        Log.add(`Falha ao clicar em "${nomeAcao}". Selector: "${selector}". Erro: ${error}`, `E`);
    };
};

exports.clickById = async function(Log, escopo, id, nomeAcao){
    try{
        let selector = "#" + id;
        return await exports.clickBySelector(Log, escopo, selector, nomeAcao);
    }catch(error){
        Log.add(`Falha ao clicar em "${nomeAcao}". Id: "${selector}. Erro: ${error}`, `E`);
    };
};

exports.clickByName = async function(Log, escopo, name, index, nomeAcao){
    try{
        await escopo.evaluate((name, index) => {
            document.getElementsByName(name)[index].click();
        }, name, index);
    }catch(error){
        Log.add(`Falha ao clicar em "${nomeAcao}". Name: "${name}". Erro: ${error}`, `E`);
    };
};

exports.clickByClassName = async function(Log, escopo, className, index, nomeAcao){
    try{
        let selector = "." + className;
        await escopo.waitFor(selector);
        await escopo.evaluate((className, index) => {
            document.getElementsByClassName(className)[index].click();
        }, className, index);
        return true;
    }catch(error){
        Log.add(`Falha ao clicar em "${nomeAcao}". Name: "${name}". Erro: ${error}`, `E`);
        return false;
    };
};

exports.clickByAttributeOfTag = async function(Log, escopo, tagName, attributeName, attributeValue, nomeAcao, waitSelector){
    try{
        if ((waitSelector) && (waitSelector != ""))
            await exports.esperarBySelector(escopo, waitSelector);
        var existiu = false;
        for (let count = 0; count < 30; count++){
            existiu = await escopo.evaluate((tagName, attributeName, attributeValue) => {
                var aTags = document.getElementsByTagName(tagName);
                for (var i = 0; i < aTags.length; i++) {
                    if (aTags[i][attributeName] == attributeValue) {
                        aTags[i].click();
                        return true;
                    };
                };
                return false;
            }, tagName, attributeName, attributeValue);

            if (existiu)
                return
            else
                await func.sleep(1000);
        };
    }catch(error){
        Log.add(`Falha ao clicar em "${nomeAcao}". TagName: "${tagName}". AttributeName: "${attributeName}". AttributeValue: "${attributeValue}". Erro: ${error}`, `E`);
    };
};

exports.getTextoSelector = async function(Log, escopo, selector){
    try{
        if (await exports.getSelectorExists(Log, escopo, selector))
            return await escopo.evaluate((selector) => {
                var element = document.querySelector(selector);
                return element? element.innerText: "";
            }, selector)
        else
            return "";
    }catch(error){
        Log.add(`Falha ao obter texto contido no selector "${selector}". Erro: ${error}`, `E`);
    };
};

exports.getTextoId = async function(Log, escopo, id){
    try{
        let selector = "#" + id;
        return await exports.getTextoSelector(Log, escopo, selector);
    }catch(error){
        Log.add(`Falha ao obter texto contido no id ${id}. Erro: ${error}`, `E`);
    };
};

exports.scrollAteFim = async function(Log, page){
    await page.evaluate(async () => {
        await new Promise((resolve, reject) => {
            var totalHeight = 0;
            var distance = 100;
            var timer = setInterval(() => {
                var scrollHeight = document.body.scrollHeight;
                window.scrollBy(0, distance);
                totalHeight += distance;

                if(totalHeight >= scrollHeight){
                    clearInterval(timer);
                    resolve();
                }
            }, 100);
        });
    });
};

exports.abrirBrowser = async function(Log, headless, usarProxy = useProxy, exibirMsg = true){
    try{
        var browser;
        var args = []; // ['--no-sandbox', '--disable-dev-shm-usage']
        if ((usarProxy) && (conf.proxy.host) && (conf.proxy.host != "") && 
            (conf.proxy.port) && (conf.proxy.port != ""))
            args.push('--proxy-server=' + conf.proxy.host + ':' + conf.proxy.port);
        var options = {ignoreHTTPSErrors: true, 
                       args: args,
                       headless: headless
                       //,executablePath: '/usr/bin/chromium-browser'
        };
        if (exibirMsg)
            Log.add(`Inicializando o Chrome...`, `I`);
        browser = await puppeteer.launch(options);
        return browser;
    }catch(error){
        Log.add(`Falha ao inicializar Chrome. Erro: ${error}`, `E`);
        return browser;
    };
};

exports.fecharBrowser = async function(Log, browser, exibirMsg = true){
    try{
        if (exibirMsg)
            Log.add(`Fechando Chrome...`, `I`);
        if ((browser != undefined) && (browser.isConnected))
            await browser.close()
        else if (exibirMsg)
            Log.add(`Browser já fechado.`, `I`);
        return true;
    }catch(error){
        return false;
    };    
};

exports.abrirPagina = async function(Log, browser, url, usarProxy = useProxy, indexPagina = 0, nroTentativas = 10, exibirMsg = true, timeOut = 180000, classRobo = '', subFolder = '', waitAfterOpen = 200){
    try{
        var page;
        if ((indexPagina == undefined) || (indexPagina == null) || (indexPagina < 0))
            indexPagina = 0;
        for (let count = 0; count < nroTentativas; count++){
            try{
                var pages = await browser.pages();
                if (pages[indexPagina] == undefined)
                    page = await browser.newPage();
                else
                    page = pages[indexPagina];
                await page.setViewport({width: 1024 , height : 800});
                await exports.retiraAlertasPage(page);
                // AUTENTICAÇÃO PROXY
                if ((usarProxy) && (conf.proxy.user) && (conf.proxy.user != "") && 
                    (conf.proxy.password) && (conf.proxy.password != ""))
                    page.authenticate({
                        username: conf.proxy.user,
                        password: conf.proxy.password
                    });
                // CAMINHO DOWNLOAD DE ARQUIVOS
                if ((classRobo != undefined) && (classRobo != '')){
                    await func.sleep(3000);
                    await exports.setDownloadPath(page, classRobo, subFolder);
                };
                // ACESSAR URL
                await page.goto(url, {timeout: timeOut});
                await func.sleep(waitAfterOpen);
                break;
            }catch(error){
                await func.sleep(3000);
                if (indexPagina > 0)
                    exports.fecharPagina(Log, page);
                page = undefined;
                if (exibirMsg)
                    Log.add(`Falha ao acessar url "${url}". Erro: ${error}`, `E`);
                if (count < nroTentativas)
                    await func.sleep(30000);
                continue;
            };
        };
        if ((page == undefined) && (exibirMsg))
            Log.add(`Falha ao acessar url "${url}"`, `E`)
        else
            await exports.retiraAlertasPage(page);
        return page;
    }catch(error){
        if (exibirMsg)
            Log.add(`Falha ao acessar url "${url}". Erro: ${error}`, `E`);
        return page;
    };
};

exports.alternarPagina = async function(Log, browser, index = 0){
    try{
        var page;
        var pages = await browser.pages();
        if (pages[index] != undefined){
            var page = pages[index];
            page.bringToFront();
        };
        if (page != undefined)
            await exports.retiraAlertasPage(page);
        return page;
    }catch(error){
        Log.add(`Falha ao alternar para aba ${index}". Erro: ${error}`, `E`);
        return page;
    };
};

exports.fecharPaginaIndex = async function(Log, browser, index){
    try{
        var pages = await browser.pages();
        var page = pages[index];
        page.close();
        return true;
    }catch(error){
        return false;
    };
};

exports.fecharPagina = async function(Log, page){
    try{
        page.close();
        return true;
    }catch(error){
        return false;
    };
};

exports.teclaPress = async function(Log, escopo, tecla){
    try{
        await escopo.keyboard.press(tecla);
    }catch(error){
        Log.add(`Falha ao pressionar tecla "${tecla}". Erro: ${error}`, `E`);
    };
};

exports.teclaDown = async function(Log, escopo, tecla){
    try{
        await escopo.keyboard.down(tecla);
    }catch(error){
        Log.add(`Falha ao descer tecla "${tecla}". Erro: ${error}`, `E`);
    };
};

exports.teclaUp = async function(Log, escopo, tecla){
    try{
        await escopo.keyboard.up(tecla);
    }catch(error){
        Log.add(`Falha ao subir tecla "${tecla}". Erro: ${error}`, `E`);
    };
};

exports.teclaCharacter = async function(Log, escopo, char){
    try{
        await escopo.keyboard.sendCharacter(char);
    }catch(error){
        Log.add(`Falha ao enviar character "${char}". Erro: ${error}`, `E`);
    };
};

exports.teclaType = async function(Log, escopo, texto){
    try{
        await escopo.keyboard.type(texto);
    }catch(error){
        Log.add(`Falha ao digitar "${texto}". Erro: ${error}`, `E`);
    };
};

exports.teclaDownString = async function(Log, escopo, texto, waitTime){
    try{
        if (waitTime == undefined)
            waitTime = 200;
        for (let index = 0; index < texto.length; index++) {
            await func.sleep(waitTime);
            if (index == (texto.length - 1))
                await exports.teclaPress(Log, escopo, texto[index]);    
            else
                await exports.teclaDown(Log, escopo, texto[index])
        };
    }catch(error){
        Log.add(`Falha ao pressionar teclas da string "${texto}". Erro: ${error}`, `E`);
    };
};

exports.teclaPressString = async function(Log, escopo, texto, waitTime){
    try{
        if (waitTime == undefined)
            waitTime = 200;
        for (let index = 0; index < texto.length; index++) {
            await func.sleep(waitTime);
            await exports.teclaPress(Log, escopo, texto[index]);
        };
    }catch(error){
        Log.add(`Falha ao pressionar teclas da string "${texto}". Erro: ${error}`, `E`);
    };
};

exports.getPropBySelector = async function(Log, escopo, selector, propriedade){
    try{
        var valorProp = '';
        if (!await exports.getSelectorExists(Log, escopo, selector, 200))
            Log.add(`Falha ao obter propriedade "${propriedade}" do selector "${selector}". Seletor inválido.`, `E`);
        else
            valorProp = await escopo.evaluate((selector, propriedade) => {
                try{
                    var valorProp = '';
                    var element   = document.querySelector(selector);
                    if (element != undefined)
                        valorProp = element[propriedade] ? element[propriedade]: '';
                }catch(error){
                    console.log(error);
                }finally{
                    return valorProp;
                };
            }, selector, propriedade);
    }catch(error){
        Log.add(`Falha ao obter propriedade "${propriedade}" do selector "${selector}". Erro: ${error}`, `E`);
    }finally{
        return valorProp;
    };
};

exports.getPropById = async function(Log, escopo, id, propriedade){
    try{
        let selector = "#" + id;
        return await exports.getPropBySelector(Log, escopo, selector, propriedade);
    }catch(error){
        Log.add(`Falha ao obter propriedade "${propriedade}" do id "${id}". Erro: ${error}`, `E`);
    };
};

exports.getPropByName = async function(Log, escopo, name, propriedade){
    try{
        let selector = `[name="${name}"]`;
        return await exports.getPropBySelector(Log, escopo, selector, propriedade);
    }catch(error){
        Log.add(`Falha ao obter propriedade "${propriedade}" do id "${id}". Erro: ${error}`, `E`);
    };
};

exports.getSelectorExists = async function(Log, escopo, selector, time = 2000){
    try{
        await escopo.waitForSelector(selector, {timeout: time});
        return true;
    }catch (error){
        if (error instanceof TimeoutError)
            return false
        else
            Log.add(`Falha ao esperar selector "${selector}. Erro: ${error}`, `E`);
    };
};

exports.getIdExists = async function(Log, escopo, id, time){
    try{
        let selector = "#" + id;
        return await exports.getSelectorExists(Log, escopo, selector, time);
    }catch (error){
        if (error instanceof TimeoutError)
            return false
        else
            Log.add(`Falha ao esperar id "${id}. Erro: ${error}`, `E`);
    };
};

exports.getNameExists = async function(escopo, name, index){
    try{
        return await escopo.evaluate((name, index) => {
            if (index == undefined)
                return (document.getElementsByName(name).length > 0)
            else
                return (document.getElementsByName(name)[index] != undefined);
        }, name, index);
    }catch (error){
        if (error instanceof TimeoutError)
            return false
        else
            Log.add(`Falha ao esperar name "${name}". Erro: ${error}`, `E`);
    };
};

exports.getTagExists = async function(escopo, tagName, index){
    try{
        return await escopo.evaluate((tagName, index) => {
            if (index == undefined)
                return (document.getElementsByTagName(tagName).length > 0)
            else
                return (document.getElementsByTagName(tagName)[index] != undefined);
        }, tagName, index);
    }catch (error){
        if (error instanceof TimeoutError)
            return false
        else
            Log.add(`Falha ao esperar tag "${tagName}. Erro: ${error}`, `E`);
    };
};

exports.getClassExists = async function(escopo, className, index){
    try{
        return await escopo.evaluate((className, index) => {
            if (index == undefined)
                return (document.getElementsByClassName(className).length > 0)
            else
                return (document.getElementsByClassName(className)[index] != undefined);
        }, className, index);
    }catch (error){
        if (error instanceof TimeoutError)
            return false
        else
            Log.add(`Falha ao esperar classe "${className}. Erro: ${error}.`, `E`);
    };
};

exports.focusBySelector = async function(Log, escopo, selector, nomeCampo){
    try{
        await escopo.evaluate((selector) => {
            document.querySelector(selector).focus();
        }, selector);
        return true;
    }catch(error){
        Log.add(`Falha ao dar foco no campo "${nomeCampo}". Selector: "${selector}". Erro: ${error}`, `E`);
        return false;
    };
};

exports.focusById = async function(Log, escopo, id, nomeCampo){
    try{
        let selector = "#" + id;
        return await exports.focusBySelector(Log, escopo, selector, nomeCampo);
    }catch(error){
        Log.add(`Falha ao dar foco no campo "${nomeCampo}". Id: "${id}". Erro: ${error}`, `E`);
    };
};

exports.focusByName = async function(Log, escopo, name, index, nomeCampo){
    try{
        await escopo.evaluate((name, index) => {
            document.getElementsByName(name)[index].focus();
        }, name, index);
    }catch(error){
        Log.add(`Falha ao dar foco no campo "${nomeCampo}". Name: "${name}. Erro: ${error}`, `E`);
    };
};

exports.setAttributteByTagName = async function(Log, escopo, tagName, index, atributo, valor){
    try{
        await escopo.evaluate((tagName, index, atributo, valor) => {
            const element = document.getElementsByTagName(tagName)[index];
            element.setAttribute(atributo, valor);
        }, tagName, index, atributo, valor);
    }catch(error){
        Log.add(`Falha ao setar atributo "${atributo}" para "${valor}". TagName: "${tabName}. Index: "${index}". Erro: ${error}`, `E`);
    };
};

exports.setAttributteBySelector = async function(Log, escopo, selector, atributo, valor){
    try{
        await escopo.evaluate((selector, atributo, valor) => {
            const element = document.querySelector(selector);
            element.setAttribute(atributo, valor);
        }, selector, atributo, valor);
    }catch(error){
        Log.add(`Falha ao setar atributo "${atributo}" para "${valor}. Selector: "${selector}". Erro: ${error}`, `E`);
    };
};

exports.setAttributteById = async function(Log, escopo, id, atributo, valor){
    try{
        let selector = "#" + id;
        return await exports.setAttributteBySelector(Log, escopo, selector, atributo, valor);
    }catch(error){
        Log.add(`Falha ao setar atributo "${atributo}" para "${valor}. Id: "${id}". Erro: ${error}`, `E`);
    };
};

exports.getAttributteBySelector = async function(Log, escopo, selector, atributo){
    try{
        return await escopo.evaluate((selector, atributo) => {
            return document.querySelector(selector).getAttribute(atributo);
        }, selector, atributo);
    }catch(error){
        Log.add(`Falha ao obter atributo "${atributo}" do selector "${selector}". Erro: ${error}`, `E`);
    };
};

exports.getAttributteByAttributteValue = async function(Log, escopo, atributo, atributoPesquisar, valorAtributoPesquisar){
    try{
        let selector = "['" + atributoPesquisar + "=" + valorAtributoPesquisar + "']";
        return await exports.getAttributteBySelector(Log, escopo, selector, atributo);
    }catch(error){
        Log.add(`Falha ao obter atributo "${atributo}" do selector "${selector}". Erro: ${error}`, `E`);
    };
};

exports.getAttributteById = async function(Log, escopo, id, atributo){
    try{
        let selector = "#" + id;
        return await exports.getAttributteBySelector(Log, escopo, selector, atributo);
    }catch(error){
        Log.add(`Falha ao obter atributo "${atributo}" do id "${id}". Erro: ${error}`, `E`);
    };
};

exports.esperarBySelector = async function(escopo, selector, timeout, mostrarMsg = true){
    try{
        if ((timeout == undefined) || (timeout == null) || (timeout <= 0))
            timeout = 30000;
        await escopo.waitForSelector(selector, {timeout: timeout});
        return true;
    }catch(error){
        if (mostrarMsg)
            console.log(error);
        return false;
    };
};

exports.esperarById = async function(escopo, id, time, mostrarMsg = true){
    try{
        let selector = "#" + id;
        return await exports.esperarBySelector(escopo, selector, time, mostrarMsg);
    }catch(error){
        if (mostrarMsg)
            console.log(error);
        return false;
    };
};

exports.esperarByFunction = async function(escopo, funcao){
    await escopo.waitForFunction(funcao);
    // ex: await page.waitForFunction('document.querySelector(".count").inner‌​Text.length == 7');
};

exports.retiraAlertasPage = async function(escopo){
    try{
        await escopo.evaluate(() => {
            window.alert = function(){};
        });
        return true;
    }catch(error){
        return false;
    };
};

exports.voltarPaginaAnterior = async function(escopo){
    try{
        await escopo.evaluate(() => {
            window.history.back();
        });
        return true;
    }catch(error){
        return false;
    };
};

exports.iniciarTracing = async function(escopo){
    try{
        await escopo.tracing.start({path: 'trace.json'});
        return true;
    }catch(error){
        return false;
    };
};

exports.pararTracing = async function(escopo){
    try{
        await escopo.tracing.stop();
        return true;
    }catch(error){
        return false;
    };
};

exports.setDownloadPath = async function(escopo, classRobo, subfolder){
    try{
        var downloadsDir = func.getDownloadPath(classRobo, subfolder);
        await escopo._client.send('Page.setDownloadBehavior', {behavior: 'allow', downloadPath: downloadsDir});
        return true;
    }catch(error){
        console.log(error);
        return false;
    };
};

exports.forcarDownloadUrlDocumento = async function(escopo, url, nomeDoc){
    try{
        var baixou = false;
        baixou     = await escopo.evaluate((url, nomeDoc) => {
            try{
                var baixou    = false;
                var link      = document.createElement('a');
                link.href     = url;
                link.download = nomeDoc;
                link.dispatchEvent(new MouseEvent('click'));
                baixou = true;
            }catch(error){
                baixou = false;
                console.log(error);
            }finally{
                return baixou;
            };
        }, url, nomeDoc);
    }catch(error){
        baixou = false;
        console.log(error);
    }finally{
        return baixou;
    };
};

exports.paginaAnterior = async function(escopo, nroVezes = 1){
    try{
        for (let index = 0; index < nroVezes; index++)
            await escopo.goBack();
        return true;
    }catch(error){
        console.log(error);
        return false;
    };
};

exports.gerarPdfUrl = async function(Log, url = '', caminhoPdf = '', headless = true, usarProxy = useProxy){
    try{
        var gerouPdf = false;
        if ((url == undefined) || (url == null) || (url == '')){
            Log.add(`Falha ao obter quantidade de "${nomeAcao}". Selector: "${selector} Erro: ${error}`, `E`);
            return gerouPdf;
        };
        var browser = await exports.abrirBrowser(Log, headless, usarProxy, false);
        if (browser != undefined){
            var page = await exports.abrirPagina(Log, browser, url, usarProxy, 0, 3, false);
            if (page != undefined){
                await page.pdf({path: caminhoPdf});
                await exports.fecharBrowser(Log, browser, false);
                gerouPdf = true;
            };
        };
    }catch(error){
        Log.add(`Falha ao gerar PDF da url "${url}". Erro: ${error}`, `E`);
        gerouPdf = false;
    }finally{
        return gerouPdf;
    };
};

exports.downloadFileBrowser = async function(Log, urlFile, nomeArquivo, classRobo, subfolder, usarProxy = useProxy, urlAcessoDoc = 'https://www.google.com/', waitAfterOpen = 200){
    try{
        var baixou = false;
        if ((urlFile == undefined) || (urlFile == null) || (urlFile == '')){
            Log.add(`${MSG_FALHA_BAIXAR_DOCUMENTO} URL do documento inválida.`, `E`);
            return baixou;
        };
        if ((nomeArquivo == undefined) || (nomeArquivo == null) || (nomeArquivo == '')){
            Log.add(`${MSG_FALHA_BAIXAR_DOCUMENTO} Nome do arquivo inválido.`, `E`);
            return baixou;
        };
        var browser = await exports.abrirBrowser(Log, true, usarProxy, false);
        if (browser == undefined){
            Log.add(`${MSG_FALHA_BAIXAR_DOCUMENTO} Browser inválido.`, `E`);
            return baixou;
        };
        var page = await exports.abrirPagina(Log, browser, urlAcessoDoc, false, 0, 3, true, 60000, classRobo, subfolder, waitAfterOpen);
        if (page == undefined){
            Log.add(`${MSG_FALHA_BAIXAR_DOCUMENTO} Página inválida.`, `E`);
            return baixou;
        };
        if (await exports.forcarDownloadUrlDocumento(page, urlFile, nomeArquivo)){
            // AGUARDA POR ARQUIVO (4 MIN)
            for (let index = 0; index < 120; index++){
                var downloadsDir = func.getDownloadPath(classRobo, subfolder);
                await func.sleep(2000);
                if (await func.arquivoExiste(downloadsDir)){
                    baixou = true;
                    break;
                };
            };
        };
    }catch(error){
        baixou = false;
        Log.add(`${MSG_FALHA_BAIXAR_DOCUMENTO} Erro: ${error}`, `E`);
    }finally{
        if (browser != undefined)
            await exports.fecharBrowser(Log, browser, false);
        return baixou;
    };
};