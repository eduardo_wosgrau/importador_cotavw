/* --------------------------------------------------------------------------
    FUNÇÕES LICITAÇÃO                                          Wosgrau
-------------------------------------------------------------------------- */
const funcoes = require('./funcoes.js');

exports.validaValoresProposta = function(Log, valorProposta = '', valorMelhorLance = '', valorNegociado = ''){
    try{
        var valoresValidos = true;
        if ((valorProposta == undefined) || (valorProposta == null) || 
            (valorProposta == '') || (!funcoes.isNumeric(Log, valorProposta)))
            valoresValidos = false;
        if ((valorMelhorLance == undefined) || (valorMelhorLance == null) || 
            (valorMelhorLance == '') || (!funcoes.isNumeric(Log, valorMelhorLance)))
            valoresValidos = false;
        if ((valorNegociado == undefined) || (valorNegociado == null) || 
            (valorNegociado == '') || (!funcoes.isNumeric(Log, valorNegociado)))
            valoresValidos = false;
    }catch(error){
        valoresValidos = false;
        Log.add(`Falha ao validar valores da proposta. Erro: ${error}`, `E`);
    }finally{
        return valoresValidos;
    };
};