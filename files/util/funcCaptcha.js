/* --------------------------------------------------------------------------
    FUNÇÕES CAPTCHA                                             Wosgrau
-------------------------------------------------------------------------- */
const request = require('request');
const fs      = require('fs');
const func    = require('./funcoes.js');
const funcPup = require('./funcPup.js');
const conf    = require('./../config/api.conf.json')

exports.getCaptcha = async function(Log, base64){
    try{
        var resolucao = '';
        var formData = {
            imagem: base64
        };
        var options = {
            url   : conf.captchaSolver.host + ':' + conf.captchaSolver.port + '/api/v1/captcha',
            method: 'GET',
            body  : formData,
            json  : true
        };
        request(options, function (error, response, body) {
            if ((!error) && (response.statusCode == 200))
                resolucao = body.data.resolution;
        });
        Log.add(`Captcha resolvido com sucesso.`, `C`);
    }catch(error){
        Log.add('Falha ao resolver captcha. Erro: ' + error, `E`);
    }finally{
        return resolucao;
    };
};

exports.getBase64BySelector = async function(Log, escopo, selector){
    try{
        var base64 = '';
        if (await funcPup.esperarBySelector(escopo, selector, 5000)){
            var path     = './captcha' + func.limpaCaracEspec(func.retiraEspacos(selector)) + '.png';
            var svgImage = await escopo.$(selector);
            await svgImage.screenshot({
                path : path,
                omitBackground: true
            });
            base64 = await fs.readFileSync(path, "base64");
            fs.unlinkSync(path);
            Log.add('Imagem (base64) do captcha obtida com sucesso.', `C`);
        }else
            Log.add('Imagem (base64) do captcha não encontrada.', `I`);
    }catch(error){
        Log.add('Falha ao obter imagem (base64) do captcha. Erro: ' + error, `E`);
    }finally{
        return base64;
    };
};

exports.getBase64ById = async function(Log, escopo, id){
    try{
        var base64   = '';
        var selector = '#' + id;
        base64       = await exports.getBase64BySelector(Log, escopo, selector);
    }catch(error){
        Log.add('Falha ao obter imagem (base64) do captcha. Erro: ' + error, `E`);
    }finally{
        return base64;
    };
};

exports.getRecaptcha = async function(Log, versao, dataSiteKey, site){
    try{
        var resultReCaptcha = '';
        if ((dataSiteKey == undefined) || (dataSiteKey == null) || (dataSiteKey.length < 10)){
            Log.add('Key do reCaptcha inválida.', `E`);
            return resultReCaptcha;
        };
        switch (versao) {
            case 1:
            case 2:
            case 3:
                promessa = new Promise(async (resolve) => {    
                    var formData = {
                        urlSite    : site,
                        dataSiteKey: dataSiteKey
                    };
                    var options = {
                        url   : conf.captchaSolver.host + ':' + conf.captchaSolver.port + '/api/v1/recaptcha',
                        method: 'GET',
                        body  : formData,
                        json  : true
                    };
                    request(options, function (err, response, body){
                        try{
                            if (err)
                                console.log(err)
                            else if ((response.statusCode == 200) && (body.data.resolution != undefined))
                                resultReCaptcha = body.data.resolution;
                        }catch(err){
                            console.log(err);
                        }finally{
                            resolve();
                        };
                    });
                });
                await Promise.all([promessa]);
            default:
                break;
        };
    }catch(error){
        Log.add('Falha ao resolver recaptcha. Erro: ' + error, `E`);
    }finally{
        return resultReCaptcha;
    };
};

exports.recarregarRecaptcha = async function(Log, escopo, versao){
    try{
        switch (versao){
            case 1:
                return await escopo.evaluate(() => {
                    try{
                        Recaptcha.reload();
                        return true;
                    }catch(error){
                        console.log(error);
                        return false;
                    };
                });
            case 2:
            case 3:
                return await escopo.evaluate(() => {
                    try{
                        grecaptcha.reset();
                        return true;
                    }catch(error){
                        return false;
                    };
                });
            default:
                return false;
        };
    }catch(error){
        Log.add('Falha ao recarregar reCaptcha. Erro: ' + error, `E`);
        return false;
    };
};

exports.setResultRecaptcha = async function(Log, escopo, resultado, versao){
    try{
        if ((resultado == undefined) || (resultado == null) || (resultado == '')){
            Log.add('Falha ao preencher resultado do reCaptcha. Resultado inválido.', `E`);
            return false;
        };
        switch (versao){
            case 2:
                // MOSTRA CAMPO DE RESULTADO DO RECAPTCHA
                await funcPup.setAttributteById(Log, escopo, 'g-recaptcha-response', 'style', 'width: 250px; height: 40px; border: 1px solid #c1c1c1; margin: 10px 25px; padding: 0px; resize: none;  display: true;');
                // PREENCHE O RESULTADO DO RECAPTCHA
                await funcPup.setBySelector(Log, escopo, '#g-recaptcha-response', resultado, 'Resultado Recaptcha');
                /* ESCONDE CAMPO DE RESULTADO DO RECAPTCHA 
                    !IMPORTANTE PARA EVITAR ERRO NA MANIPULAÇÃO DE OUTROS COMPONENTES DA TELA */
                await funcPup.setAttributteById(Log, escopo, 'g-recaptcha-response', 'style', 'width: 250px; height: 40px; border: 1px solid #c1c1c1; margin: 10px 25px; padding: 0px; resize: none; display: none;float: right;');
                return true;
            default:
                return false;
        };
    }catch(error){
        Log.add('Falha ao preencher resultado do reCaptcha. Erro: ' + error, `E`);
        return false;
    };
};