/* --------------------------------------------------------------------------
    ROTAS                                                       Wosgrau
-------------------------------------------------------------------------- */
const conf         = require('./api.conf.json');
const defaultRoute = conf.server.urlBase;

module.exports = app => {
    /*app.get(defaultRoute + 'robos/:classe/', app.api.v1.controllers.Robo.robosGET)
    app.get(defaultRoute + 'robos/:classe/:param1', app.api.v1.controllers.Robo.robosGET)
    app.get(defaultRoute + 'robos/:classe/logs', app.api.v1.controllers.Log.logsGET)
    app.get(defaultRoute + 'robos/:classe/logs/:nomeLog', app.api.v1.controllers.Log.nomeLogGET)*/
    app.get(defaultRoute + 'VwFileReader/:tipo/:nomeArquivo', app.api.v1.controllers.VwFileReader.VwFileReaderGET)
};