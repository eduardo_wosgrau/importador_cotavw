/* --------------------------------------------------------------------------
    index.js                                                   Wosgrau
-------------------------------------------------------------------------- */
// const db   = require('./config/db')
// app.db     = db
const app     = require('express')()
const consign = require('consign')
const conf    = require('./config/api.conf.json')
const log     = require('./classes/v1/log.js');

global.nrTentatRobo            = 5;
global.nroPublicacoesVerificar = 10000;
global.headless                = false;
global.useProxy                = true;

consign()
    .then('./config/middlewares.js')
    .then('./util/validation.js')
    .then(conf.server.dirApi)
    .then('./config/routes.js')
    .into(app)

app.listen(conf.server.port, () => {
    var Log = new log();
    Log.add(`Servidor ativo. [PORTA: ${conf.server.port}]`, `I`);
});