/* --------------------------------------------------------------------------
    FACTORY                                                    Wosgrau
-------------------------------------------------------------------------- */
const func    = require('../../util/funcoes.js');
const funcPup = require('../../util/funcPup.js');
const Robos   = {};

module.exports = class factory{
    constructor(Log, classRobo, param, param1){
        try{
            this.classRobo = classRobo.toLowerCase();
            this.param     = param;
            this.param1    = param1;
            Log.add(`Instanciando robô...`, `I`);
            this.objRobo = new Robos[classRobo](Log, this.classRobo, param, param1);
            if (!this.objRobo)
                Log.add(`Falha ao instanciar robô.`, `E`);
        }catch(error){
            Log.add(`Falha ao instanciar robô. Erro: ${error} .`, `E`);
        };
    };

    validarParam(Log, param, param1){
        try{
            Log.add(`Validando parâmetros da requisição...`, `I`);
            if (!this.objRobo.validarParam(Log, param, param1))
                return false;
            return true;
        }catch(error){
            Log.add(`Falha ao validar parâmetros da requisição. Erro: ${error}.`, `E`);
        };
    };

    async execute(Log){
        try{
            var resultado = {};
            if (this.objRobo){
                Log.add(`Executando robô...`, `I`);
                var dtInicio = Date.now();
                for (let count = 0; count < nrTentatRobo; count++){
                    try{
                        Log.add(`---------------------------------- Tentativa ${(count + 1)} -----------------------------------`, `I`);
                        if (this.objRobo.abrirBrowser){
                            var browser = await funcPup.abrirBrowser(Log, headless, this.objRobo.usarProxy, true);
                            if ((browser != undefined) && (await this.objRobo.acessarSite(Log, browser)))
                                resultado = await this.objRobo.execute(Log, browser);
                        }else
                            resultado = await this.objRobo.execute(Log);
                        break;
                    }catch(error){
                        continue;
                    }finally{
                        if (this.objRobo.abrirBrowser)
                            await funcPup.fecharBrowser(Log, browser, true);
                    };
                };
                var duracao = func.getDateDiff(Log, dtInicio, Date.now())
                Log.add(`Execução finalizada. Tempo Total: ${duracao}`, `I`);
            }else
                Log.add(`Falha ao executar robô. Não instânciado. Erro: ${error}.`, `E`);
        }catch(error){
            Log.add(`Falha ao executar robô. Erro: ${error}.`, `E`);
        }finally{
            return resultado;
        };
    };
};