/* --------------------------------------------------------------------------
    CLASSE QUERY CONSULTA                                      Wosgrau
-------------------------------------------------------------------------- */
const funcMsSQL = require('../../util/funcMsSQL.js');

module.exports = class qryConsulta{
    constructor(query, params = {}, msgErro = '', nomeConfDb = ''){
        try{
            this.query      = query;
            this.params     = params;
            this.data       = {};
            this.msgErro    = msgErro;
            this.nomeConfDb = nomeConfDb;
        }catch(error){
            console.log(`Falha ao criar qryConsulta. Erro: ${error}`);
        };
    };

    addData(nome, objeto){
        try{
            this.data[nome] = objeto;
            return true;
        }catch(error){
            console.log(`Falha ao adicionar data "${nome}" no qryConsulta. Erro: ${error}`);
            return false;
        };  
    };

    delData(nome){
        try{
            delete this.data[nome];
            return true;
        }catch(error){
            console.log(`Falha ao excluir data "${nome}" do qryConsulta. Erro: ${error}`);
            return false;
        };  
    };

    async execute(Log){
        return await funcMsSQL.executeQuery(Log, this);
    };
};