/* --------------------------------------------------------------------------
    CLASSE API RESPONSE                                        Wosgrau
-------------------------------------------------------------------------- */
module.exports = class classApiResponse{
    constructor(){
        try{
            this.apiResponse = {
                "messages": [],
                "data"    : {}
            };
        }catch(error){
            console.log(`Falha ao criar apiResponse. Erro: ${error}`, `E`);
        };
    };
    addMessage(help, description, type){
        try{
            var message         = util.message;
            message.help        = help;
            message.description = description;
            message.type        = type;
            this.apiResponse.messages.push(message);
            return true;
        }catch(error){
            console.log(`Falha ao adicionar uma mensagem. Erro: ${error}`);
            return false;
        };   
    };
    clearMessages(){
        try{
            this.apiResponse.messages = [];
            return true;
        }catch(error){
            console.log(`Falha ao adicionar uma mensagem. Erro: ${error}`);
            return false;
        };
    };
    addData(nome, objeto){
        try{
            this.apiResponse.data[nome] = objeto;
            return true;
        }catch(error){
            console.log(`Falha ao adicionar objeto na apiResponse. Erro: ${error}`);
            return false;
        };  
    };
    delData(nome){
        try{
            delete this.apiResponse.data[nome];
            return true;
        }catch(error){
            console.log(`Falha ao adicionar objeto na apiResponse. Erro: ${error}`);
            return false;
        };  
    };
};