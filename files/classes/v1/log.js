/* --------------------------------------------------------------------------
    CLASSE LOG                                                  Wosgrau
-------------------------------------------------------------------------- */
const moment = require('moment');
const fs     = require('fs');

module.exports = class log{
    constructor(className, nroExec){
        try{
            this.log        = [];
            this.idExecucao = 0;
            // GRAVA ARQUIVO .LOG
            if (process.cwd().indexOf('/') >= 0){
                this.diretorio = process.cwd().replace("files", "logs/");
                if (!fs.existsSync(this.diretorio))
                    fs.mkdirSync(this.diretorio);
                if ((className != undefined) && (className != '')){
                    this.diretorio = this.diretorio + className + "/";
                    if (!fs.existsSync(this.diretorio))
                        fs.mkdirSync(this.diretorio);
                };
            }else if (process.cwd().indexOf('\\') >= 0){
                this.diretorio = process.cwd().replace("files", "logs\\");
                if (!fs.existsSync(this.diretorio))
                    fs.mkdirSync(this.diretorio);
                if ((className != undefined) && (className != '')){
                    this.diretorio = this.diretorio + className + "\\";
                    if (!fs.existsSync(this.diretorio))
                        fs.mkdirSync(this.diretorio);
                };
            };
            this.arquivo = "log_" + moment(Date.now()).format("DD_MM_YYYY_hh_mm_ss");
            if ((nroExec != undefined) && (nroExec > 0))
                this.arquivo += "_exec_" + nroExec.toString();
            this.arquivo += ".txt";
        }catch(error){
            console.log(`Falha ao criar JSON de Log. Erro: ${error}`, `E`);
        };
    };

    /* TIPOS:   [C] - OK
                [E] - ERRO
                [A] - ADVERTÊNCIA
                [I] - INFORMAÇÃO */
    async add(descricao, tipo){
        try{
            // MONTAGEM
            var strData = moment(Date.now()).format(`DD/MM/YYYY HH:mm:ss`);
            var strLog  = "[" + strData + "] [" + tipo + "] " + descricao;
            // LOG
            console.log(strLog);
            // GRAVA ARQUIVO .LOG
            this.log.push({data: strData, tipo: tipo, descricao: descricao});
            fs.mkdir(this.diretorio, { recursive: true }, (err) => {
                if (err) console.log(err);
            });
            fs.appendFile(this.diretorio + this.arquivo, strLog + "\n", (err) => {
                if (err) console.log(err);
            });
            return true;
        }catch(error){
            console.log(`Falha ao adicionar log no JSON. Erro: ${error}`, `E`);
            return false;
        };
    };

    limpar(){
        try{
            this.log       = [];
            this.arquivo   = '';
            this.diretorio = '';
            return;
        }catch(error){
            console.log(`Falha ao limpar JSON de log. Erro: ${error}`, `E`);
        };
    };

    async setIdExecucao(idExecucao, idRobo = null){
        try{
            this.idExecucao = idExecucao;
            this.idRobo     = idRobo;
        }catch(error){
            console.log(error);
        };
    };
};