/* --------------------------------------------------------------------------
    SERVICE VW FILE READER                                      Wosgrau
-------------------------------------------------------------------------- */
const funcoes = require('../../../util/funcoes.js');
const awsService = require('./awsService.js');

const MSG_NENHUM_MODELO_ENCONTRADO          = `Nenhum modelo encontrado no arquivo.`;
const MSG_PAGINA_IMPORTADA_COM_SUCESSO      = `Página importada com sucesso.`;
const MSG_FALHA_IMPORTAR_ARQUIVO            = `Falha ao importar arquivo.`;
const MSG_FALHA_AO_LER_PAGINA               = `Falha ao ler página do Pdf.`;
const MSG_FALHA_AO_OBTER_COD_PAGINA         = `Falha ao obter código da página.`;
const MSG_FALHA_AO_OBTER_COLS_OPT           = `Falha ao obter colunas do grid de opcionais do arquivo Pdf.`;
const MSG_FALHA_AO_OBTER_OPCIONAIS          = `Falha ao obter opcionais do arquivo Pdf.`;
const MSG_FALHA_AO_OBTER_DADOS_MODELOS      = `Falha ao obter dados dos modelos do arquivo Pdf.`;
const MSG_FALHA_AO_OBTER_DADOS_DA_PAGINA    = `Falha ao obter dados da página do arquivo Pdf.`;
const MSG_FALHA_CONSOLIDAR_JSON_PAGINA      = `Falha ao consolidar json da página;`;
const MSG_OBJETO_PAGINA_IMPORTACAO_INVALIDO = `Objeto página da importação inválido.`;
const MSG_ARRAY_DE_DADOS_INVALIDO           = `Array de dados inválido.`;
const STATUS_ARQUIVO_IMPORTADO_SUCESSO      = `Arquivo importado com sucesso.`;
const STATUS_FALHA_BAIXAR_ARQUIVO_S3        = `Falha ao baixar arquivo do Bucket S3.`;

const STATUS_CODE_SUCCESS = 1;
const STATUS_CODE_FAILURE = 9;

exports.importarArquivo = async function(Log, tipo, nomeArquivo){
    try{
        // ESTRUTURA DE RETORNO
        var resultadoImportacao = {
            status_code       : '',
            status_description: '',
            data              : {}
        };
        // VALIDAÇÕES
        if ((tipo == undefined) || (tipo == null) || (tipo == '')){
            resultadoImportacao.status_code        = STATUS_CODE_FAILURE;
            resultadoImportacao.status_description = `${MSG_FALHA_IMPORTAR_ARQUIVO} Tipo do arquivo inválido.`;
            Log.add(`${MSG_FALHA_IMPORTAR_ARQUIVO} Tipo do arquivo inválido.`, `E`);
            return resultadoImportacao;
        };
        if ((nomeArquivo == undefined) || (nomeArquivo == null) || (nomeArquivo == '')){
            resultadoImportacao.status_code        = STATUS_CODE_FAILURE;
            resultadoImportacao.status_description = `${MSG_FALHA_IMPORTAR_ARQUIVO} Nome do arquivo inválido.`;
            Log.add(`${MSG_FALHA_IMPORTAR_ARQUIVO} Nome do arquivo inválido.`, `E`);
            return resultadoImportacao;
        };
        // BAIXA ARQUIVO PDF DO BUCKET S3
        var caminhoPdf = await baixarArquivoS3(Log, tipo, nomeArquivo);
        if ((caminhoPdf == undefined) || (caminhoPdf == null) || (caminhoPdf == '')){
            resultadoImportacao.status_code        = STATUS_CODE_FAILURE;
            resultadoImportacao.status_description = STATUS_FALHA_BAIXAR_ARQUIVO_S3;
            Log.add(STATUS_FALHA_BAIXAR_ARQUIVO_S3, `E`);
            return resultadoImportacao;
        };
        var txtImportacao = {paginas: []};        
        var nroPagina     = 0;
        var jsonPdf       = await funcoes.pdfToJson(Log, caminhoPdf); // return jsonPdf.formImage.Pages;
        var Paginas       = jsonPdf.formImage.Pages;
        // PERCORRE PÁGINAS
        for (let idxPagina = 0; idxPagina < Paginas.length; idxPagina++){
            try{
                Log.add(`Página ${(idxPagina + 1)}`, `I`);
                var Dados  = Paginas[idxPagina];
                var pagina = {
                    metaDados: {
                        titulo      : '',
                        codigoPagina: '',
                        emissao     : '',
                        validade    : '',
                        icms        : ''
                    },
                    colsGridOpt: [],
                    opcionais: []
                };
                // CÓDIGO DA PÁGINA
                if (!getCodigoPagina(Log, pagina, Dados))
                    Log.add(MSG_FALHA_AO_OBTER_COD_PAGINA, `I`);
                // COLUNAS GRID OPCIONAIS
                if (!getColsOpt(Log, pagina, Dados)){
                    Log.add(MSG_FALHA_AO_OBTER_COLS_OPT, `E`);
                    break;
                };
                // CÓDIGOS DOS OPCIONAIS
                if (!getOpcionais(Log, pagina, Dados)){
                    Log.add(MSG_FALHA_AO_OBTER_OPCIONAIS, `E`);
                    break;
                };
                // DADOS DOS MODELOS
                if (!getDadosModelos(Log, pagina, Dados)){
                    Log.add(MSG_FALHA_AO_OBTER_DADOS_MODELOS, `E`);
                    break;
                };
                // DADOS DA PÁGINA
                if (!getDadosPagina(Log, pagina, Dados)){
                    Log.add(MSG_FALHA_AO_OBTER_DADOS_DA_PAGINA, `E`);
                    break;
                };
                // CONSOLIDAR DADOS PÁGINA
                if (!consolidarJsonPagina(Log, pagina)){
                    Log.add(MSG_FALHA_CONSOLIDAR_JSON_PAGINA, `E`);
                    break;
                };
            }catch(error){
                Log.add(`${MSG_FALHA_AO_LER_PAGINA} Página: ${(nroPagina + 1)} Erro: ${error}`, `E`);
            }finally{
                if ((pagina.colsGridOpt == undefined) && (pagina.opcinoais == undefined) && (pagina.modelos.length > 0)){
                    txtImportacao.paginas.push(pagina);
                    Log.add(MSG_PAGINA_IMPORTADA_COM_SUCESSO, `I`);
                }else{
                    Log.add(MSG_NENHUM_MODELO_ENCONTRADO, `I`);
                };
                Log.add(`----------------------------------------------------------------------------------`, `I`);
            };
        };
    }catch(error){
        Log.add(`${MSG_FALHA_IMPORTAR_ARQUIVO} Erro: ${error}`, `E`);
    }finally{
        if (txtImportacao.paginas.length > 0){
            resultadoImportacao.status_code        = STATUS_CODE_SUCCESS;
            resultadoImportacao.status_description = STATUS_ARQUIVO_IMPORTADO_SUCESSO;
            resultadoImportacao.data               = txtImportacao;
            Log.add(STATUS_ARQUIVO_IMPORTADO_SUCESSO, `C`);
        }else
            Log.add(MSG_NENHUM_MODELO_ENCONTRADO, `I`);
        Log.add(`----------------------------------------------------------------------------------`, `I`);
        return resultadoImportacao;
    };
};

function getCodigoPagina(Log, pagina, Dados){
    try{
        var pegouCodigoPagina = false;
        if ((pagina == undefined)){
            Log.add(`${MSG_FALHA_AO_OBTER_COD_PAGINA} ${MSG_OBJETO_PAGINA_IMPORTACAO_INVALIDO}`, `I`);
            return pegouCodigoPagina;
        };
        if ((Dados == undefined) || (Dados == [])){
            Log.add(`${MSG_FALHA_AO_OBTER_COD_PAGINA} ${MSG_ARRAY_DE_DADOS_INVALIDO}`, `I`);
            return pegouColOpt;
        };
        var regCodigoPagina = /^\b([A-Z])\w{5}\b/g;
        for (idxDado = 0; idxDado < Dados.Texts.length; idxDado++){
            var txtLinha = Dados.Texts[idxDado].R[0].T;
            txtLinha     = decodeURIComponent(txtLinha);
            txtLinha     = txtLinha.trim();
            if ((txtLinha != undefined) && (txtLinha != null) && (txtLinha != '')){
                if (funcoes.numericBetween(Log, Dados.Texts[idxDado].y, 1, 1.2)){
                    var isCodigoPagina = regCodigoPagina.test(txtLinha);
                    if (isCodigoPagina){
                        pagina.metaDados.codigoPagina = txtLinha.trim();
                        pegouCodigoPagina             = true;
                        break;
                    };
                };
            };
        };
    }catch(error){
        pegouCodigoPagina = false;
        Log.add(`${MSG_FALHA_AO_OBTER_COD_PAGINA} Erro: ${error}`, `E`);
    }finally{
        return pegouCodigoPagina;
    };
};

function getColsOpt(Log, pagina, Dados){
    try{
        var pegouColOpt = false;
        if ((pagina == undefined)){
            Log.add(`${MSG_FALHA_AO_OBTER_COLS_OPT} ${MSG_OBJETO_PAGINA_IMPORTACAO_INVALIDO}`, `I`);
            return pegouColOpt;
        };
        if ((Dados == undefined) || (Dados == [])){
            Log.add(`${MSG_FALHA_AO_OBTER_COLS_OPT} ${MSG_ARRAY_DE_DADOS_INVALIDO}`, `I`);
            return pegouColOpt;
        };
        var yIni = 14;
        var yFim = 15;
        // 3 LINHAS DO NOME DAS COLUNAS
        for (let idxLinha = 0; idxLinha <= 2; idxLinha++, yIni++, yFim++){
            var idxCol = 0;
            for (var idxDado = 0; idxDado < Dados.Texts.length; idxDado++){
                if (funcoes.numericBetween(Log, Dados.Texts[idxDado].y, yIni, yFim)){
                    var txtLinha = Dados.Texts[idxDado].R[0].T;
                    txtLinha     = decodeURIComponent(txtLinha);
                    txtLinha     = txtLinha.trim();
                    if ((txtLinha != undefined) && (txtLinha != null) && (txtLinha != '')){
                        // 1ª LINHA
                        if (idxLinha == 0)
                            pagina.colsGridOpt.push([txtLinha])
                        // 2ª E 3ª LINHA
                        else
                            pagina.colsGridOpt[idxCol].push(txtLinha);
                        idxCol++;
                    };
                };
            };
        };
        pegouColOpt = true;
    }catch(error){
        pegouColOpt = false;
        Log.add(`${MSG_FALHA_AO_OBTER_COLS_OPT} Erro: ${error}`, `E`);
    }finally{
        return pegouColOpt;
    };
};

function getOpcionais(Log, pagina, Dados){
    try{
        var pegouOpt = false;
        if ((pagina == undefined)){
            Log.add(`${MSG_FALHA_AO_OBTER_OPCIONAIS} ${MSG_OBJETO_PAGINA_IMPORTACAO_INVALIDO}`, `I`);
            return pegouOpt;
        };
        if ((Dados == undefined) || (Dados == [])){
            Log.add(`${MSG_FALHA_AO_OBTER_OPCIONAIS} ${MSG_ARRAY_DE_DADOS_INVALIDO}`, `I`);
            return pegouOpt;
        };
        for (var idxDado = 0; idxDado < Dados.Texts.length; idxDado++){
            if ((Dados.Texts[idxDado].y > 17) && (funcoes.numericBetween(Log, Dados.Texts[idxDado].x, 1.0, 1.1))){
                var idxCol   = 0;
                var cordY    = Dados.Texts[idxDado].y;
                var opcional = {
                    pacote : '',
                    nome   : '',
                    valores: []
                };
                for (var idxDadoOpt = 0; idxDadoOpt < Dados.Texts.length; idxDadoOpt++){
                    if (Dados.Texts[idxDadoOpt].y == cordY){
                        var txtLinha = Dados.Texts[idxDadoOpt].R[0].T;
                        txtLinha     = decodeURIComponent(txtLinha);
                        txtLinha     = txtLinha.trim();
                        if ((txtLinha != undefined) && (txtLinha != null) && (txtLinha != '')){
                            // PACOTE
                            if (idxCol == 0)
                                opcional.pacote = txtLinha
                            // DESCRIÇÃO
                            else if (idxCol == 1){
                                opcional.nome = txtLinha;
                                // VALORES
                                var cordYVlrIni = (cordY + 0.08);
                                var cordYVlrFim = (cordY + 0.15);
                                for (var idxDadoOptVlr = 0; idxDadoOptVlr < Dados.Texts.length; idxDadoOptVlr++){
                                    if (funcoes.numericBetween(Log, Dados.Texts[idxDadoOptVlr].y, cordYVlrIni, cordYVlrFim)){
                                        var txtLinha = Dados.Texts[idxDadoOptVlr].R[0].T;
                                        txtLinha     = decodeURIComponent(txtLinha);
                                        txtLinha     = txtLinha.trim();
                                        if ((txtLinha != undefined) && (txtLinha != null) && (txtLinha != ''))
                                            opcional.valores.push(txtLinha);
                                    };
                                };
                            };
                            idxCol++;
                        };
                    };
                };
                if (opcional.pacote != '')
                    pagina.opcionais.push(opcional);
            };
        };
        pegouOpt = true;
    }catch(error){
        pegouOpt = false;
        Log.add(`${MSG_FALHA_AO_OBTER_OPCIONAIS} Erro: ${error}`, `E`);
    }finally{
        return pegouOpt;
    };
};

function getDadosModelos(Log, pagina, Dados){
    try{
        var pegouDadosModelos = false;
        if ((pagina == undefined)){
            Log.add(`${MSG_FALHA_AO_OBTER_DADOS_MODELOS} ${MSG_OBJETO_PAGINA_IMPORTACAO_INVALIDO}`, `I`);
            return pegouDadosModelos;
        };
        if ((Dados == undefined) || (Dados == [])){
            Log.add(`${MSG_FALHA_AO_OBTER_DADOS_MODELOS} ${MSG_ARRAY_DE_DADOS_INVALIDO}`, `I`);
            return pegouDadosModelos;
        };
        var modelCodes    = [];
        var descricoes = [];
        var edicoes    = [];
        var valores    = [];
        for (var idxDado = 0; idxDado < Dados.Texts.length; idxDado++){
            if ((Dados.Texts[idxDado].y > 6) && (Dados.Texts[idxDado].y < 17)){
                var txtLinha = Dados.Texts[idxDado].R[0].T;
                txtLinha     = decodeURIComponent(txtLinha);
                txtLinha     = txtLinha.trim();
                if ((txtLinha != undefined) && (txtLinha != null) && (txtLinha != '')){
                    // CÓDIGO
                    if (funcoes.numericBetween(Log, Dados.Texts[idxDado].x, 1.00, 1.03)){ // if (Dados.Texts[idxDado].x == 1.01){
                        if (txtLinha != 'Código')
                            modelCodes.push(txtLinha);
                    // DESCRIÇÃO
                    }else if (funcoes.numericBetween(Log, Dados.Texts[idxDado].x, 3.62, 3.65)){ // if (Dados.Texts[idxDado].x == 3.635){
                        if (txtLinha != 'Descrição')
                            descricoes.push(txtLinha);
                    // DESCRIÇÃO
                    }else if (funcoes.numericBetween(Log, Dados.Texts[idxDado].x, 3.65, 5)){ // if (Dados.Texts[idxDado].x == 3.635){
                        if ((txtLinha != 'Descrição') && (descricoes.length > 0))
                            descricoes[(descricoes.length - 1)] += txtLinha;
                    // EDIÇÃO
                    }else if (funcoes.numericBetween(Log, Dados.Texts[idxDado].x, 17.68, 17.88)){ // if (Dados.Texts[idxDado].x == 17.78){
                        if (txtLinha != 'Edição')
                            edicoes.push(txtLinha);
                    // R$
                    }else if (funcoes.numericBetween(Log, Dados.Texts[idxDado].x, 17.97, 19.17)){ // if (Dados.Texts[idxDado].x == 19.07){
                        if (txtLinha != 'R$')
                            valores.push(txtLinha);
                    // R$ COMPLEMENTO
                    }else if (Dados.Texts[idxDado].x > 19.17){
                        if ((txtLinha != 'R$') && (valores.length > 0) && (valores[(valores.length - 1)].indexOf('.') < 0))
                            valores[(valores.length - 1)] += txtLinha;
                    };
                };
            };
        };
        // ORGANIZA MODELOS
        var modelos = [];
        for (idxCod = 0; idxCod < modelCodes.length; idxCod++){
            var valor = valores[idxCod];
            valor     = valor.replace(/,/g, '');
            var modelo = {
                modelCode: modelCodes[idxCod],
                modelo    : descricoes[idxCod],
                edicao    : edicoes[idxCod],
                valor     : valor
            };
            modelos.push(modelo);
        };
        pagina.modelos = modelos;
        pegouDadosModelos = true;
    }catch(error){
        pegouDadosModelos = false;
        Log.add(`${MSG_FALHA_AO_OBTER_DADOS_MODELOS} Erro: ${error}`, `E`);
    }finally{
        return pegouDadosModelos;
    };
};

function getDadosPagina(Log, pagina, Dados){
    try{
        var pegouDadosPagina = false;
        if ((pagina == undefined)){
            Log.add(`${MSG_FALHA_AO_OBTER_DADOS_DA_PAGINA} ${MSG_OBJETO_PAGINA_IMPORTACAO_INVALIDO}`, `I`);
            return pegouDadosPagina;
        };
        if ((Dados == undefined) || (Dados == [])){
            Log.add(`${MSG_FALHA_AO_OBTER_DADOS_DA_PAGINA} ${MSG_ARRAY_DE_DADOS_INVALIDO}`, `I`);
            return pegouDadosPagina;
        };
        var regData    = /^([0-2][0-9]|(3)[0-1])(\/)(((0)[0-9])|((1)[0-2]))(\/)\d{4}$/i;
        var regEmissao = /\b\d{1,5}\/\d{1,5}\b/g;
        for (idxDado = 0; idxDado < Dados.Texts.length; idxDado++){
            var txtLinha = Dados.Texts[idxDado].R[0].T;
            txtLinha     = decodeURIComponent(txtLinha);
            txtLinha     = txtLinha.trim();
            if ((txtLinha != undefined) && (txtLinha != null) && (txtLinha != '') && (txtLinha != '---------')){
                if (funcoes.numericBetween(Log, Dados.Texts[idxDado].x, 4, 5)){
                    var isData    = regData.test(txtLinha);
                    var isEmissao = regEmissao.test(txtLinha);
                    if (isData)
                        pagina.metaDados.validade = txtLinha
                    else if (isEmissao)
                        pagina.metaDados.emissao = txtLinha
                    else
                        pagina.metaDados.icms = txtLinha;
                }else if (funcoes.numericBetween(Log, Dados.Texts[idxDado].y, 3.6, 3.8))
                    pagina.metaDados.titulo = txtLinha;
                // PEGOU DADOS OK
                if ((pagina.metaDados.validade != '') && (pagina.metaDados.emissao != '') && 
                    (pagina.metaDados.icms != '') && (pagina.metaDados.titulo != '')){
                    pegouDadosPagina = true;
                    break;
                };
            };
        };
    }catch(error){
        pegouDadosPagina = false;
        Log.add(`${MSG_FALHA_AO_OBTER_DADOS_DA_PAGINA} Erro: ${error}`, `E`);
    }finally{
        return pegouDadosPagina;
    };
};

function consolidarJsonPagina(Log, pagina){
    try{
        var consolidarOk = false;
        if (pagina == undefined){
            Log.add(`${MSG_FALHA_AO_OBTER_DADOS_DA_PAGINA} ${MSG_OBJETO_PAGINA_IMPORTACAO_INVALIDO}`, `I`);
            return consolidarOk;
        };
        // MODELOS
        for (let idxModelo = 0; idxModelo < pagina.modelos.length; idxModelo++){
            // COLUNAS GRID OPCIONAIS
            pagina.modelos[idxModelo].opcionais = [];
            for (let idxColGridOpt = 0; idxColGridOpt < pagina.colsGridOpt.length; idxColGridOpt++) {
                // LINHAS COLUNA GRID OPCIONAL
                var achouColsModelo = false;
                for (let idxLinhaCol = 0; idxLinhaCol < pagina.colsGridOpt[idxColGridOpt].length; idxLinhaCol++){
                    if (pagina.colsGridOpt[idxColGridOpt][idxLinhaCol] == pagina.modelos[idxModelo].modelCode){
                        // OPCIONAIS
                        for (let idxOpt = 0; idxOpt < pagina.opcionais.length; idxOpt++){
                            var opcional       = {};
                            opcional.pacote = pagina.opcionais[idxOpt].pacote;
                            opcional.nome   = pagina.opcionais[idxOpt].nome;
                            opcional.valor  = pagina.opcionais[idxOpt].valores[idxColGridOpt];
                            pagina.modelos[idxModelo].opcionais.push(opcional);
                        };
                        achouColsModelo = true;
                        break;
                    };
                };
                if (achouColsModelo)
                    break;
            };
        };
        consolidarOk = true;
    }catch(error){
        consolidarOk = false;
        Log.add(`${MSG_FALHA_CONSOLIDAR_JSON_PAGINA} Erro: ${error}`, `E`);
    }finally{
        delete pagina.colsGridOpt;
        delete pagina.opcionais;
        return consolidarOk;
    };
};

async function baixarArquivoS3(Log, tipo, nomeArquivo){
    try{
        var caminhoArquivo = '';
        if ((tipo == undefined) || (tipo == null) || (tipo == '')){
            Log.add(`${STATUS_FALHA_BAIXAR_ARQUIVO_S3} Tipo do arquivo inválido.`, `E`);
            return caminhoArquivo;
        };
        if ((nomeArquivo == undefined) || (nomeArquivo == null) || (nomeArquivo == '')){
            Log.add(`${STATUS_FALHA_BAIXAR_ARQUIVO_S3} Nome do arquivo inválido.`, `E`);
            return caminhoArquivo;
        };
        var pastaTipoArq = ''
        if (tipo == 'Lista de Preços')
            pastaTipoArq = 'lista_de_precos';
        if ((nomeArquivo == undefined) || (nomeArquivo == null) || (nomeArquivo == '')){
            Log.add(`${STATUS_FALHA_BAIXAR_ARQUIVO_S3} Pasta do tipo não definida.`, `E`);
            return caminhoArquivo;
        };
        var caminhoS3      = `importacoes/${pastaTipoArq}/${nomeArquivo}`;
        var caminhoDestino = process.cwd().replace("files", "arq\\") + nomeArquivo;
        if (await awsService.downloadFile(Log, caminhoS3, caminhoDestino))
            caminhoArquivo = caminhoDestino;
    }catch(error){
        Log.add(`${STATUS_FALHA_BAIXAR_ARQUIVO_S3} Erro: ${error}`, `E`);
    }finally{
        return caminhoArquivo;
    };
};