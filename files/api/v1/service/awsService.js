/* --------------------------------------------------------------------------
    SERVICE AWS                                                 Wosgrau
-------------------------------------------------------------------------- */
const AWS             = require('aws-sdk')
const async           = require('async')
const conf            = require('./../../../config/api.conf.json');
const func            = require('./../../../util/funcoes.js');
const fs              = require('fs')
const downloadsFolder = require('downloads-folder');
const s3              = new AWS.S3({
  accessKeyId    : conf.bucketAWS.iamUserKey,
  secretAccessKey: conf.bucketAWS.iamUserSecret,
  Bucket         : conf.bucketAWS.bucketName,
  region         : conf.bucketAWS.region
});
let arquivo, nomeArq, nomeArqDest;

const createMainBucket = (callback) => {
	const params = {
    Bucket: conf.bucketAWS.bucketName
	};                    
	s3.headBucket(params, function(err, data){
	  if (err){
      console.log("ErrorHeadBucket", err);
      s3.createBucket(params, function(err, data) {
      if (err){
        console.log("Error", err);
        callback(err, null);
      }else
        callback(null, data);
			});
    }else
      callback(null, data);
	});
};

const createItemObject = (callback) => {
  const params = { 
    Bucket: conf.bucketAWS.bucketName, 
    Key   : conf.bucketAWS.directory + `${nomeArqDest}`,
    ACL   : 'public-read',
    Body  : arquivo
  };
	s3.putObject(params, function (err, data) {
		if (err) {
      console.log("Erro ao fazer upload do arquivo: ", err);
      callback(err, null)
	  }else{
      // console.log("Upload do arquivo concluído com sucesso.", data);
      callback(null, data)
	  };
	});
};

exports.uploadToS3 = async function (nomeArquivo, nomeArquivoDest, caminho) {
  var promessa = new Promise(async (resolve, reject) => {
    try{
      var tmp_path = caminho;
      arquivo      = fs.createReadStream(tmp_path);
      nomeArq      = nomeArquivo;
      nomeArqDest  = nomeArquivoDest;
      async.series([
        createMainBucket,
        createItemObject
      ], (err, result) => {
        if (err){
          console.log(err);
          resolve(false);
        }else{
          // console.log('Upload concluído com sucesso.')
          resolve(true);
        };
      });
    }catch(error){
      Log.add(`Falha ao fazer upload do arquivo para o S3. Erro: ${error}`, `E`);
      resolve(false);
    };
  });
  return await Promise.resolve(promessa);
};

exports.uploadDownloadedFileToS3 = async function (Log, nomeArquivo, nomeArquivoDest, classRobo, subFolder = '', tempoMaxEsperaArq = 120000) {
  try{
    // VALIDAÇÕES
    if ((nomeArquivo == undefined) || (nomeArquivo == null) || (nomeArquivo == '')){
      Log.add(`Falha ao fazer upload do arquivo baixado para o S3. Nome do arquivo inválido`);
      return false;
    };
    // CAMINHO ARQUIVO LOCAL
    var caminhoLocal = '';
    if ((classRobo == undefined) || (classRobo == null) || (classRobo == ''))
      caminhoLocal = downloadsFolder()
    else{
      caminhoLocal = func.getDownloadPath(classRobo);
      if ((subFolder != undefined) && (subFolder != '')){
        if (caminhoLocal.indexOf('/') >= 0)
          caminhoLocal += '/' + subFolder + '/'
        else
          caminhoLocal += '\\' + subFolder + '\\';
      };
    };
    if (caminhoLocal.indexOf('/') >= 0)
      caminhoLocal += '/' + nomeArquivo
    else
      caminhoLocal += '\\' + nomeArquivo;
    // NOME DO ARQUIVO DESTINO
    var caminhoDest = '';
    if ((classRobo != undefined) && (classRobo != null) && (classRobo != '')){
        caminhoDest = classRobo;
        if ((subFolder != undefined) && (subFolder != ''))
          caminhoDest += '/' + subFolder + '/';
    };
    if ((nomeArquivoDest == undefined) || (nomeArquivoDest == null) || (nomeArquivoDest == ''))
        nomeArquivoDest = nomeArquivo;
      caminhoDest += nomeArquivoDest;
    // AGUARDA POR ARQUIVO
    var downloadOK = false;
    var iteracoes = (tempoMaxEsperaArq / 1000);
    for (let index = 0; index < iteracoes; index++) {
      if (await func.arquivoExiste(caminhoLocal)){
        // SOBE ARQUIVO PARA O S3
        downloadOK = await exports.uploadToS3(nomeArquivo, caminhoDest, caminhoLocal);
        break;
      };
      await func.sleep(1000);
    };
    return downloadOK;
  }catch(error){
    Log.add(`Falha ao fazer upload do arquivo baixado para o S3. Erro: ${error}`, `E`);
    return false;
  };
};

// DOWNLOAD ARQUIVO
const filePath = './data/downloaded.json';
const key      = 'data/data.json';

exports.downloadFile = async function (Log, caminhoS3, filePath){
  try{
    var promessa = new Promise(async (resolve, reject) => {
      const params = {
        Bucket: conf.bucketAWS.bucketName,
        Key   : caminhoS3
      };
      //Key: secretAccessKey  
      s3.getObject(params, (err, data) => {
        try{
          if (err){
            console.error(err);
            resolve(false);
          }else if ((data == undefined) || (data.Body == undefined))
            resolve(false)
          else{
            var writeStream = fs.createWriteStream(filePath);
            writeStream.write(data.Body);
            resolve(true);
          };
        }catch(error){
          console.log(error);
          resolve(false);
        };
      });
    });
    return await Promise.resolve(promessa);
  }catch(error){
    Log.add(`Erro: ${error}`, `E`);
    return false;
  };
};