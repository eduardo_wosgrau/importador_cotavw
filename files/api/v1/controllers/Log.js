/* --------------------------------------------------------------------------
    LOG                                                         Wosgrau
-------------------------------------------------------------------------- */
const fs = require('fs');

module.exports = app => {
  const logsGET = async (req, res, next) => {
    try{
      var linksLogs    = '';
      var resposta     = '';
      var classRobo    = req.params.classe.toLowerCase();
      var path;
      if (process.cwd().indexOf('/') >= 0)
        path = process.cwd().replace("files", "logs/") + classRobo + "/";
      else if (process.cwd().indexOf('\\') >= 0)
        path = process.cwd().replace("\\files", "\\logs\\") + classRobo + "\\";
      var promiseArray = [];
      var promessa     = new Promise(async (resolve, reject) => {
        try{
          fs.readdir(path, 'utf-8', async (err, files) => {
            if (err)
              resolve([]);
            else{
              files.forEach(element => {
                  promiseArray.push(new Promise(async (resolve, reject) => {
                    resolve(element);
                  }));
              });
              resolve(await Promise.all(promiseArray));
            };
          });
        }catch(error){
          resolve([]);
        };
      });
      arquivosLog = await Promise.resolve(promessa);
      for (let count = 0; count < arquivosLog.length; count++)
        linksLogs += `<a href="#" onclick="window.location = (window.location + \'/` + arquivosLog[count] + `\').replace(\'#\', \'\').replace(\'logs//\', \'logs/\')";>` + arquivosLog[count] + `</a></br>`;
      resposta = `<!DOCTYPE html>
        <html>
          <body>
            <div>
              <h1>Logs ` + classRobo + `</h1>` + 
              linksLogs + `
            <\div>
          </body>
        </html>`;
      res.setHeader('Content-Lenght', resposta.length);
      res.write(resposta, 'binary');
      res.end();
    }catch(error){
      console.log("Falha ao obter logs. Erro: ${error}.");
      res.send("");
    };
  };

  const nomeLogGET = async (req, res, next) => {
    try{
      var conteudoLog = '';
      var arquivo     = req.params.nomeLog.toLowerCase();
      var classRobo   = req.params.classe.toLowerCase();
      var path;
      if (process.cwd().indexOf('/') >= 0)
        path = process.cwd().replace("files", "logs/") + classRobo + "/";
      else if (process.cwd().indexOf('\\') >= 0)
        path = process.cwd().replace("\\files", "\\logs\\") + classRobo + "\\";
      path = path + arquivo;
      var promessa    = new Promise(async (resolve, reject) => {
        try{
          fs.readFile(path, 'utf-8', async (err, data) => {
            if (!err)
              resolve(data)
            else
              resolve('');
          });
        }catch(error){
          resolve('');
        };
      });
      conteudoLog = await Promise.resolve(promessa);
      res.setHeader('Content-Lenght', conteudoLog.length);
      res.write(conteudoLog, 'binary');
      res.end();
    }catch(error){
      console.log("Falha ao obter logs. Erro: ${error}.");
      res.send("");
    };
  };

  return { logsGET, nomeLogGET }
};