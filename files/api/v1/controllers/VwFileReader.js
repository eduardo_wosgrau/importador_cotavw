/* --------------------------------------------------------------------------
    VW PDF READER                                              Wosgrau
-------------------------------------------------------------------------- */
const VwFileReaderService = require('../service/VwFileReaderService.js');
const log                 = require('../../../classes/v1/log.js');

const MSG_FALHA_INICIAR_IMPORTACAO_PDF = `Falha ao iniciar importação do arquivo PDF.`;
var nroExec                            = 0;

module.exports = app => {
  const VwFileReaderGET = async (req, res, next) => {
    try{
      var Log         = new log('VwFileReader', nroExec);
      var tipo        = req.params.tipo;
      var nomeArquivo = req.params.nomeArquivo;
      Log.add(`----------------------------------------------------------------------------------`, `I`);
      Log.add(`-- Importação | Tipo: "${tipo}" Nome do Arquivo: "${nomeArquivo}"`, `I`);
      Log.add(`----------------------------------------------------------------------------------`, `I`);
      var response = await VwFileReaderService.importarArquivo(Log, tipo, nomeArquivo);
    }catch(error){
      response = `${MSG_FALHA_INICIAR_IMPORTACAO_PDF} Erro: ${error}`;
      Log.add(response, `E`);
    }finally{      
      res.setHeader('Content-Lenght', 'application/json');
      res.send(response);
    };
  };

  return { VwFileReaderGET }
};