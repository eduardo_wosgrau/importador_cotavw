/* --------------------------------------------------------------------------
    ROBÔ                                                        Wosgrau
-------------------------------------------------------------------------- */
const factory = require('../../../classes/v1/factory.js');
const log     = require('../../../classes/v1/log.js');
var nroExec   = 0;

module.exports = app => {
  const robosGET = async (req, res, next) => {
    try{
      var classRobo = req.params.classe;
      var param     = req.query;
      var param1    = req.params.param1;
      nroExec++;
      var Log = new log(classRobo, nroExec);
      Log.add('----------------------------------------------------------------------------------', `I`);
      Log.add('-- Robô: ' + classRobo + ' | Nro execução: ' + nroExec.toString(), `I`);
      Log.add('----------------------------------------------------------------------------------', `I`);
      var obj = new factory(Log, classRobo, param, param1);
      var msg = '';
      if (obj.validarParam(Log, param, param1)){
        obj.execute(Log, nroExec);
        msg = 'Execução ' + nroExec.toString() + ' do robô ' + classRobo + ' instanciada com sucesso.';
      }else{
        msg = 'Parâmetros incorretos! Operação cancelada.';
        Log.add(msg, `E`);
      }
    }catch(error){
      msg = 'Falha na montagem do robô. Erro: ' + error + '.';
      Log.add(msg, `E`);
    }finally{      
      res.setHeader('Content-Lenght', 'application/json');
      res.send(msg);
    };
  };

  return { robosGET }
};