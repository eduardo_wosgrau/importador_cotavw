﻿# Importador
Importador de arquivos CotaVw desenvolvido em Node.js.
As configurações estão no arquivo config/api.conf.json
Para instalar pacotes utilizados: npm install
Para executar aplicação: npm start

### RODAR
sudo docker ps
sudo docker images
sudo docker rmi -f 847915f586fe
sudo docker build --rm -f "Dockerfile" -t importador:v1 .
sudo docker run --rm -d -p 8888:8888 importador:v1
sudo docker run --rm -d -e db_host=host.com.br -e db_database=banco -e db_user=usuario -e db_password=senha -p 8888:8888 1638135/red_importador:v1

### CHAMAR
http://localhost:8889
sudo docker ps
sudo docker logs -f f0bae2cb4d71

###PARAR
sudo docker ps
sudo docker stop f0bae2cb4d71
sudo docker rm f0bae2cb4d71

### SUBIR
sudo docker build --rm -f "Dockerfile" -t importador:v1 .
sudo docker tag importador:v1 1638135/red_importador:v1
sudo docker push 1638135/red_importador:v1

### ATUALIZAR NO CLIENTE
docker stop $(docker ps -aq)
docker rm $(docker ps -aq)
sudo docker pull 1638135/red_importador:v1
sudo docker rmi $(docker images --filter "dangling=true" -q --no-trunc)
sudo docker run --rm -d -e db_host=VAR_HOST -e db_database= VAR_DATABASE -e db_user= VAR_USER -e db_password=VAR_SENHA -p 8888:8888 1638135/red_importador:v1
